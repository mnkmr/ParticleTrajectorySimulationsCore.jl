using ParticleTrajectorySimulationsCore
using Documenter

makedocs(;
    modules=[ParticleTrajectorySimulationsCore],
    authors="Masaaki Nakamura",
    repo="https://gitlab.com/mnkmr/ParticleTrajectorySimulationsCore.jl/blob/{commit}{path}#L{line}",
    sitename="ParticleTrajectorySimulationsCore.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://mnkmr.gitlab.io/ParticleTrajectorySimulationsCore.jl",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
    ],
)
