# ParticleTrajectorySimulationsCore

<!-- [![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://mnkmr.gitlab.io/ParticleTrajectorySimulationsCore.jl/dev) -->
[![Build Status](https://gitlab.com/mnkmr/ParticleTrajectorySimulationsCore.jl/badges/master/pipeline.svg)](https://gitlab.com/mnkmr/ParticleTrajectorySimulationsCore.jl/pipelines)
[![Coverage](https://gitlab.com/mnkmr/ParticleTrajectorySimulationsCore.jl/badges/master/coverage.svg)](https://gitlab.com/mnkmr/ParticleTrajectorySimulationsCore.jl/commits/master)

## Introduction

This package provides functions to carry out trajectory simulations of polar molecules in Stark state selectors.


## Installation

Press `]` to start [Pkg REPL mode](https://docs.julialang.org/en/v1/stdlib/Pkg/), then execute:

```
registry add https://gitlab.com/mnkmr/MNkmrRegistry.git
add ParticleTrajectorySimulationsCore
```


## Usage

See
- [HowToUse.ipynb](https://nbviewer.org/urls/gitlab.com/mnkmr/ParticleTrajectorySimulationsCore.jl/-/raw/master/HowToUse.ipynb)
- [HowToUse_ja.ipynb](https://nbviewer.org/urls/gitlab.com/mnkmr/ParticleTrajectorySimulationsCore.jl/-/raw/master/HowToUse_ja.ipynb)
- [HowToReadResultsInPython_ja.ipynb](https://nbviewer.org/urls/gitlab.com/mnkmr/ParticleTrajectorySimulationsCore.jl/-/raw/master/HowToReadResultsInPython_ja.ipynb)
