module Progress

using Printf: @sprintf

export
    CountProgress,
    simulation_next!,
    record_next!,
    finish!


abstract type CountState end
struct Enabled <: CountState end
struct Disabled <: CountState end


# CountProgress do not handle exclusive control unlike ProgressMeter.jl.
mutable struct CountProgress{CS<:CountState,T}
    n::T
    simulated::Int
    recorded::Int
    printcol::Int
    t0::Float64
    tprev::Float64
    printed::Bool
    in_ijulia::Bool
    io::IO
    dt::Float64

    function CountProgress(n::Union{Nothing,Integer}, io::IO, dt::Real, show::Bool)
        @assert dt ≥ 0
        CS = ifelse(show, Enabled, Disabled)
        col = isnothing(n) ? 8 : (floor(Int, log10(n) + 1))
        now = time()
        in_ijulia = is_in_IJulia()
        return new{CS,typeof(n)}(n, 0, 0, col, now, now, false, in_ijulia, io, dt)
    end
end

CountProgress(n; io=stderr, dt=1.0, show=true) = CountProgress(n, io, dt, show)


is_in_IJulia() = isdefined(Main, :IJulia) && Main.IJulia.inited


function simulation_next!(cp::CountProgress{Enabled,T}) where {T<:Union{Nothing,Integer}}
    cp.simulated += 1
    if elapsed_from(cp.tprev) ≥ cp.dt
        update_display(cp)
        flush(cp.io)
    end
    return
end


function record_next!(cp::CountProgress{Enabled,T}) where {T<:Union{Nothing,Integer}}
    cp.recorded += 1
    if elapsed_from(cp.tprev) ≥ cp.dt
        update_display(cp)
        flush(cp.io)
    end
    return
end


function finish!(cp::CountProgress{Enabled,T}) where {T<:Union{Nothing,Integer}}
    if cp.printed || (elapsed_from(cp.t0) ≥ cp.dt)
        update_display(cp)
        printstyled(cp.io, "✓", color=:light_green)
        println(cp.io)
        flush(cp.io)
    end
    return
end


# Do nothing if disabled.
function simulation_next!(cp::CountProgress{Disabled,T}) where {T<:Union{Nothing,Integer}} end
function record_next!(cp::CountProgress{Disabled,T}) where {T<:Union{Nothing,Integer}} end
function finish!(cp::CountProgress{Disabled,T}) where {T<:Union{Nothing,Integer}} end


function update_display(cp::CountProgress)
    cp.printed && clearline(cp.io, cp.in_ijulia)
    print_counts(cp.io, cp.n, cp.simulated, cp.recorded, cp.printcol)
    print(cp.io, "   ")
    print_elapsed(cp.io, cp.t0)
    cp.printed = true
    cp.tprev = time()
    return
end


function clearline(io::IO, in_ijulia::Bool)
    if in_ijulia
        Main.IJulia.clear_output(true)
    else
        print(io, "\u1b[1G\u1b[K")
    end
    return
end


function print_counts(io::IO, ::Nothing, simulated::Integer, recorded::Integer, ::Integer)
    msg = @sprintf("  Simulated %8d,   Recorded %8d", simulated, recorded)
    printstyled(io, msg, color=:light_blue)
    return
end

function print_counts(io::IO, n::Integer, simulated::Integer, recorded::Integer, printcol::Integer)
    str_simulated = lpad(simulated, printcol, " ")
    str_recorded = lpad(recorded, printcol, " ")
    percentage_simulated = simulated / n * 100
    percentage_recorded = recorded / n * 100
    msg = @sprintf("  Simulated: %s/%d (%5.1f%%),   Recorded: %s/%d (%5.1f%%)",
        str_simulated, n, percentage_simulated, str_recorded, n, percentage_recorded)
    printstyled(io, msg, color=:light_blue)
    return
end


function print_elapsed(io::IO, t0::Float64)
    elapsed = elapsed_from(t0)
    elapsed_str = duration_to_string(elapsed)
    printstyled(io, elapsed_str, color=:default)
    return
end


function elapsed_from(t0)
    now = time()
    return now - t0
end


function duration_to_string(dt)
    dt = floor(Int, dt)
    d = div(dt, 24 * 60 * 60)
    dt -= d * 24
    h = div(dt, 60 * 60)
    dt -= h * 60
    m = div(dt, 60)
    dt -= m * 60
    s = dt
    d_str = d != 0 ? @sprintf("%d day ", d) : ""
    str = @sprintf("%s%02d:%02d:%02d", d_str, h, m, s)
    return str
end


end
