module Electrodes

using LinearAlgebra:
    ⋅,
    norm,
    normalize

using OrdinaryDiffEq:
    ODEProblem,
    SecondOrderODEProblem,
    remake,
    solve,
    DPRKN6,
    ContinuousCallback,
    terminate!,
    ReturnCode

using Printf:
    @printf

using RecursiveArrayTools:
    ArrayPartition

using Rotations:
    Rotation,
    RotZYZ

using StaticArrays:
    SVector

using ..Outputs:
    AbstractOutput,
    record

using ..Apparatuses:
    Apparatuses,
    Vector3D,
    Object,
    crossed_at,
    Plane,
    Circle,
    Instrument,
    pass!

using ..Particles:
    AbstractParticle,
    BasicSymmetricTop,
    mass,
    effective_dipole


function relative_coordinates(r, obj::Object)
    q = obj.orientation
    r′ = q\(r .- obj.coordinates)
    return r′
end


abstract type Electrode <: Instrument end


function Base.:(==)(a::T, b::T) where {T<:Electrode}
    exclude = Set((:problem,))
    return all(getproperty(a, key) == getproperty(b, key) for key in fieldnames(T) if !(key ∈ exclude))
end


function eom(ddu, du, u, p, t)
    electrode, particle = p
    E, ∇E = efield(electrode, u)
    μeff = effective_dipole(particle, E)
    F = @. μeff*∇E
    m = mass(particle)
    @. ddu = F/m
    return
end


struct Hexapole{T<:Real,S<:Rotation,SYM,VAL} <: Electrode
    coordinates::SVector{3,T}
    orientation::S
    len::T
    R::T
    V::T
    problem::ODEProblem
    opt_integ::NamedTuple{SYM,VAL}
    function Hexapole(coordinates::SVector{3,T}, orientation::S,
                      len::T, R::T, V::T, opt_integ::NamedTuple{SYM,VAL}) where {T<:Real,S<:Rotation,SYM,VAL}
        any(isnan, coordinates) && throw(DomainError("Invalid coordinates."))
        any(isnan, orientation) && throw(DomainError("Invalid orientation."))
        du0 = zeros(T, 3)
        u0 = zeros(T, 3)
        tspan = zeros(T, 2)
        problem = SecondOrderODEProblem(eom, du0, u0, tspan)
        return new{T,S,SYM,VAL}(coordinates, orientation, len, R, V, problem, opt_integ)
    end
end

function Hexapole(coordinates::SVector{3,T}, orientation::S,
                  len::T, R::T, V::T) where {T<:Real,S<:Rotation}
    return Hexapole(coordinates, orientation, len, R, V, NamedTuple())
end

function Hexapole(coordinates::Vector3D, orientation::Rotation, len::Real, R::Real, V::Real)
    length(coordinates) != 3 && throw(DomainError("The length of \"coordinates\" should be 3."))
    len_T, R_T, V_T, coordinates_T... = float.(promote(len, R, V, coordinates...))
    coordinates_SVector = SVector(coordinates_T[1], coordinates_T[2], coordinates_T[3])
    return Hexapole(coordinates_SVector, orientation, len_T, R_T, V_T)
end

"""
    Hexapole(coordinates, len::Real, R::Real, V::Real)
    Hexapole(coordinates, orientation, len::Real, R::Real, V::Real)

A hexapolar state selector focusing low field seekers.

`coordinates` is an iterator that have 3 items corresponding to the center
[xc, yc, zc] of the inscribed circle of six rods at the entrance.

`orientation` is an iterator having 3 items corresponding to Euler angles in
ZYZ convention. It will be (0, 0, 0) if omitted.

`len` is the length of the hexapole.

`R` is the radius of the inscribed circle of six rods.

`V` is the absolute value of the added voltage.
"""
function Hexapole(coordinates::Vector3D, orientation::Vector3D, len::Real, R::Real, V::Real)
    length(orientation) != 3 && throw(DomainError("The length of \"orientation\" should be 3."))
    orientation_RotZYZ = RotZYZ(orientation[1], orientation[2], orientation[3])
    return Hexapole(coordinates, orientation_RotZYZ, len, R, V)
end

function Hexapole(coordinates::Vector3D, len::Real, R::Real, V::Real)
    return Hexapole(coordinates, (0, 0, 0), len, R, V)
end


function Base.show(io::IO, ::MIME"text/plain", z::Hexapole)
    @printf(io, "Hexapole: Len=%e, R=%f, V=%f\n", z.len, z.R, z.V)
end


function efield(hexapole::Hexapole, u)
    x′, y′, z′ = relative_coordinates(u, hexapole)
    R = hexapole.R
    len = hexapole.len
    if !(0 ≤ z′ ≤ len && x′^2 + y′^2 ≤ R^2)
        o = zero(x′)
        return o, (o, o, o)
    end

    r′ = SVector(x′, y′, zero(z′))
    V = hexapole.V
    E = norm(@. 3*V*r′^2/R^3)
    q = hexapole.orientation
    ∇E = q*(@. 6*V*r′/R^3)
    return E, ∇E
end


function Apparatuses.pass!(u, hexapole::Hexapole, particle::AbstractParticle, out::AbstractOutput)
    circ_in = Circle(hexapole.coordinates, hexapole.orientation, hexapole.R)
    crossed, uc = crossed_at(circ_in, u)
    if !crossed
        !isnothing(uc) && record(out, uc)
        return false
    end

    vx, vy, vz = hexapole.orientation*SVector(0, 0, 1)
    ev = normalize(SVector(vx, vy, vz))
    v0 = uc[5:7]
    v = ev ⋅ v0
    Δt = hexapole.len/v
    t = uc[1]
    tspan = (t, t + Δt)

    R² = hexapole.R^2
    condition = function (u, t, integrator)
        x = u[4]
        y = u[5]
        z = u[6]
        r² = x^2 + y^2
        return R² - r²
    end
    affect!(integrator) = terminate!(integrator)
    callback = ContinuousCallback(condition, affect!, interp_points=10)

    T = eltype(uc)
    _du0 = Vector{T}(undef, 3)
    _u0 = Vector{T}(undef, 3)
    _du0 .= uc[5:7]
    _u0 .= uc[2:4]
    u0 = ArrayPartition(_du0, _u0)
    prob = remake(hexapole.problem; u0=u0, tspan=tspan, p=(hexapole, particle))
    opt_integ = hexapole.opt_integ
    sol = solve(prob, DPRKN6(); callback=callback, opt_integ...)
    # Do not use successful_retcode(), since it returns true if a trajectory is terminated.
    passed = sol.retcode == ReturnCode.Success
    for i in 1:size(sol, 2)
        t = sol.t[i]
        vx = sol[1, i]
        vy = sol[2, i]
        vz = sol[3, i]
        x = sol[4, i]
        y = sol[5, i]
        z = sol[6, i]
        utraj = (t, x, y, z, vx, vy, vz)
        record(out, utraj)
    end

    u[1] = sol.t[end]
    u[2] = sol[4, end]
    u[3] = sol[5, end]
    u[4] = sol[6, end]
    u[5] = sol[1, end]
    u[6] = sol[2, end]
    u[7] = sol[3, end]
    return passed
end


struct Deflector{T<:Real,S<:Rotation,SYM,VAL} <: Electrode
    coordinates::SVector{3,T}
    orientation::S
    inlet::Plane{T}
    len::T
    r0::T
    a::NTuple{3,T}
    Vrod::T
    Vbed::T
    problem::ODEProblem
    opt_integ::NamedTuple{SYM,VAL}
    function Deflector(coordinates::SVector{3,T}, orientation::S, inlet::Plane{T},
                       len::T, r0::T, a::NTuple{3,T}, Vrod::T, Vbed::T,
                       opt_integ::NamedTuple{SYM,VAL}) where {T<:Real,S<:Rotation,SYM,VAL}
        !all(isfinite, coordinates) && throw(DomainError("All items in coordinates should be finite numbers."))
        du0 = zeros(T, 3)
        u0 = zeros(T, 3)
        tspan = zeros(T, 2)
        problem = SecondOrderODEProblem(eom, du0, u0, tspan)
        return new{T,S,SYM,VAL}(coordinates, orientation, inlet, len, r0, a, Vrod, Vbed, problem, opt_integ)
    end
end

function Deflector(coordinates::SVector{3,T}, orientation::S, inlet::Plane{T},
                   len::T, r0::T, a::NTuple{3,T}, Vrod::T, Vbed::T) where {T<:Real,S<:Rotation}
    return Deflector(coordinates, orientation, inlet, len, r0, a, Vrod, Vbed, NamedTuple())
end

function Deflector(coordinates::Vector3D, orientation::Rotation, len::Real, r0::Real, a_series, Vrod::Real, Vbed::Real)
    length(coordinates) != 3 && throw(DomainError("The length of \"coordinates\" should be 3."))
    args = float.(promote(coordinates..., len, r0, Vrod, Vbed, a_series...))
    coordinates_SVector = SVector(args[1], args[2], args[3])
    pl = Plane(coordinates_SVector, orientation)
    len_T = args[4]
    r0_T = args[5]
    Vrod_T = args[6]
    Vbed_T = args[7]
    a_len = length(a_series)
    a_begin = 8
    a_end = a_begin + a_len - 1
    a_T = args[a_begin:a_end]
    return Deflector(coordinates_SVector, orientation, pl, len_T, r0_T, a_T, Vrod_T, Vbed_T)
end

"""
    Deflector(coordinates, len::Real, r0::Real, a_series, Vrod::Real, Vbed::Real)
    Deflector(coordinates, orientation, len::Real, r0::Real, a_series, Vrod::Real, Vbed::Real)

A conventional type of dipolar state selector.

`coordinates` is an iterator that have 3 items corresponding to the center
[xc, yc, zc] of the inscribed circle of six rods at the entrance.

`orientation` is an iterator having 3 items corresponding to Euler angles in
ZYZ convention. It will be (0, 0, 0) if omitted.

`len` is the length of the hexapole.

`r0` is a scaling parameter.

`a_series` is a1, a2, and a3 parameters.

`Vrod` and `Vbed` are the voltages added to the two electrodes.
"""
function Deflector(coordinates::Vector3D, orientation::Vector3D, len::Real, r0::Real, a_series, Vrod::Real, Vbed::Real)
    length(orientation) != 3 && throw(DomainError("The length of \"orientation\" should be 3."))
    orientation_RotZYZ = RotZYZ(orientation[1], orientation[2], orientation[3])
    return Deflector(coordinates, orientation_RotZYZ, len, r0, a_series, Vrod, Vbed)
end

function Deflector(coordinates::Vector3D, len::Real, r0::Real, a_series, Vrod::Real, Vbed::Real)
    return Deflector(coordinates, (0, 0, 0), len, r0, a_series, Vrod, Vbed)
end


function Base.show(io::IO, ::MIME"text/plain", z::Deflector)
    @printf(io, "Deflector: Len=%e, Vrod=%f, Vbed=%f\n", z.len, z.Vrod, z.Vbed)
end


function epotential_without_scaling(deflector::Deflector, u)
    x, y, _ = relative_coordinates(u, deflector)
    r0 = deflector.r0
    a1, a2, a3 = deflector.a
    return a1*y/r0 + a2*(y^2 - x^2)/(2*r0^2) + a3*(y^3 - 3*y*x^2)/(3*r0^3)
end


function epotential(deflector::Deflector, u)
    Vrod = deflector.Vrod
    Vbed = deflector.Vbed
    Φ0 = (Vrod - Vbed)/2
    offset = Φ0 + Vbed
    return Φ0*epotential_without_scaling(deflector, u) + offset
end


function efield(deflector::Deflector, u)
    x, y, z = relative_coordinates(u, deflector)
    len = deflector.len
    if !(0 ≤ z ≤ len)
        o = zero(x)
        return o, (o, o, o)
    end

    Vrod = deflector.Vrod
    Vbed = deflector.Vbed
    Φ0 = (Vrod - Vbed)/2
    a1, a2, a3 = deflector.a
    r0 = deflector.r0
    r0² = r0*r0
    r0³ = r0²*r0
    Φx′ = Φ0*(-a2*x/r0² - a3*2*y*x/r0³)
    Φy′ = Φ0*(a1/r0 + a2*y/r0² + a3*(y^2 - x^2)/r0³)
    E = hypot(Φx′, Φy′)
    E0 = Φ0/r0*a1
    Ex′ = E0*(((a2/a1)^2 - 2*a3/a1)*x/r0² - ((a2/a1)^3 - 4*a2*a3/a1^2)*x*y/r0³)
    Ey′ = E0*(a2/(a1*r0) + 2*a3*y/(a1*r0²) - (0.5*(a2/a1)^3 - 2*a2*a3/a1^2)*x^2/r0³)
    ∇E = SVector(Ex′, Ey′, zero(E0))
    q = deflector.orientation
    ∇E = q*∇E
    return E, ∇E
end


function is_in_deflector(deflector::Deflector, u::AbstractArray)
    V = epotential_without_scaling(deflector, view(u, 2:4))
    return abs(V) ≤ 1
end

function is_in_deflector(deflector::Deflector, u::NTuple)
    V = epotential_without_scaling(deflector, u[2:4])
    return abs(V) ≤ 1
end


function Apparatuses.pass!(u, deflector::Deflector, particle::AbstractParticle, out::AbstractOutput)
    pl = deflector.inlet
    crossed, uc = crossed_at(pl, u)
    if !crossed
        !isnothing(uc) && record(out, uc)
        return false
    end
    if !is_in_deflector(deflector, uc)
        !isnothing(uc) && record(out, uc)
        return false
    end

    vx, vy, vz = deflector.orientation*SVector(0, 0, 1)
    ev = normalize(SVector(vx, vy, vz))
    v0 = uc[5:7]
    v = ev ⋅ v0
    Δt = deflector.len/v
    t = uc[1]
    tspan = (t, t + Δt)

    condition = function (u, t, integrator)
        x = u[4]
        y = u[5]
        z = u[6]
        V = epotential_without_scaling(deflector, (x, y, z))
        return abs(V) - 1
    end
    affect!(integrator) = terminate!(integrator)
    callback = ContinuousCallback(condition, affect!, interp_points=10)

    T = eltype(uc)
    _du0 = Vector{T}(undef, 3)
    _u0 = Vector{T}(undef, 3)
    _du0 .= uc[5:7]
    _u0 .= uc[2:4]
    u0 = ArrayPartition(_du0, _u0)
    prob = remake(deflector.problem; u0=u0, tspan=tspan, p=(deflector, particle))
    opt_integ = deflector.opt_integ
    sol = solve(prob, DPRKN6(); callback=callback, opt_integ...)
    # Do not use successful_retcode(), since it returns true if a trajectory is terminated.
    passed = sol.retcode == ReturnCode.Success
    for i in 1:size(sol, 2)
        t = sol.t[i]
        vx = sol[1, i]
        vy = sol[2, i]
        vz = sol[3, i]
        x = sol[4, i]
        y = sol[5, i]
        z = sol[6, i]
        utraj = (t, x, y, z, vx, vy, vz)
        record(out, utraj)
    end

    u[1] = sol.t[end]
    u[2] = sol[4, end]
    u[3] = sol[5, end]
    u[4] = sol[6, end]
    u[5] = sol[1, end]
    u[6] = sol[2, end]
    u[7] = sol[3, end]

    if !is_in_deflector(deflector, u)
        return false
    end
    return passed
end


end
