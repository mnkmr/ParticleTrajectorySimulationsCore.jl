module Particles

const c = 299792458
const h = 6.62607015e-34
const NA = 6.022140857e23
const Debye2SI = 1 / c * 1e-21
const wavnum2Hz = 1e2 * c


"""
    AbstractParticle

An abstract type of particle for trajectry simulations. A particle, an instance
of its subtype `p`, should have a method `mass(p)` which returns the mass of
the particle in *kg* unit. Additionally, `effective_dipole(p::AbstractParticle, E)`
method should be available with the magnitude of electric field `E`.
"""
abstract type AbstractParticle end


mass(p::AbstractParticle) = p.mass


"""
    AsymmetricTop <: AbstractParticle

An abstract type of an asymmetric top molecule.
"""
abstract type AsymmetricTop <: AbstractParticle end


"""
    BasicAsymmetricTop{T<:Real} <: AsymmetricTop

A concrete type of an asymmetric top molecule.
"""
struct BasicAsymmetricTop{T<:Real} <: AsymmetricTop
    name::String
    mass::T
    rotA::T
    rotB::T
    rotC::T
    dipA::T
    dipB::T
    dipC::T

    """
        BasicAsymmetricTop(name::AbstractString, mass_au::Real,
                           rotA::Real, rotB::Real, rotC::Real,
                           dipA::Real, dipB::Real, dipC::Real)

    A constructor of BasicAsymmetricTop. `mass_au` should be given in
    *atomic unit*. `rotA`, `rotB`, and `rotC` should be given in wavenumber.
    `dipA`, `dipB`, and `dipC` should be given in Debye.
    """
    function BasicAsymmetricTop(
        name::AbstractString, mass_au::Real,
        rotA::Real, rotB::Real, rotC::Real,
        dipA::Real, dipB::Real, dipC::Real,
    )
        mass = mass_au * 1e-3 / NA
        _mass, _rotA, _rotB, _rotC, _dipA, _dipB, _dipC =
            float.(
                promote(
                    mass,
                    rotA * wavnum2Hz,
                    rotB * wavnum2Hz,
                    rotC * wavnum2Hz,
                    dipA * Debye2SI,
                    dipB * Debye2SI,
                    dipC * Debye2SI,
                )
            )
        T = typeof(mass)
        return new{T}(name, _mass, _rotA, _rotB, _rotC, _dipA, _dipB, _dipC)
    end
end


"""
    SymmetricTop <: AsymmetricTop

An abstract type of an symmetric top molecule.
"""
abstract type SymmetricTop <: AsymmetricTop end


"""
    BasicSymmetricTop{T<:Real,S<:Real} <: SymmetricTop

A concrete type of an symmetric top molecule.
"""
struct BasicSymmetricTop{T<:Real,S<:Real} <: SymmetricTop
    name::String
    mass::T
    rotB::T
    dipB::T
    J::S
    K::S
    M::S

    """
        BasicSymmetricTop(name::AbstractString, mass_au::Real, rotB::Real,
                          dipB::Real, J::Real, K::Real, M::Real)

    A constructor of BasicSymmetricTop. `mass_au` should be given in
    *atomic unit*. `rotB` should be given in wavenumber. `dipB` should be given
    in Debye.
    """
    function BasicSymmetricTop(
        name::AbstractString,
        mass_au::Real,
        rotB::Real,
        dipB::Real,
        J::Real,
        K::Real,
        M::Real,
    )
        mass = mass_au * 1e-3 / NA
        _mass, _rotB, _dipB = float.(promote(mass, rotB * wavnum2Hz, dipB * Debye2SI))
        _J, _K, _M = promote(J, K, M)
        T = typeof(_mass)
        S = typeof(_J)
        return new{T,S}(name, _mass, _rotB, _dipB, _J, _K, _M)
    end
end


function effective_dipole(particle::BasicSymmetricTop, E)
    # Ohoyama et al. J. Phys. Chem., Vol. 99, No. 37, 1995 13607
    B = particle.rotB
    μ = particle.dipB
    J = particle.J
    K = particle.K
    M = particle.M

    if J > 0
        ρ = K * M / (J * (J + 1))
        f = (J^2 - K^2) * (J^2 - M^2) / (J^3 * (2J - 1) * (2J + 1)) -
            ((J + 1)^2 - K^2) * ((J + 1)^2 - M^2) / ((J + 1)^3 * (2J + 1) * (2J + 3))
        μeff = μ * ρ - μ^2 * f / (B * h) * E
    elseif J == 0
        μeff = μ^2 / (3 * B * h) * E
    else
        @error "Total angular momentum J should be equal to or larger than zero."
    end
    return μeff
end


struct BasicSymmetricTop1stOrder{T<:Real,S<:Real} <: SymmetricTop
    name::String
    mass::T
    dipB::T
    J::S
    K::S
    M::S

    function BasicSymmetricTop1stOrder(
        name::AbstractString,
        mass_au::Real,
        dipB::Real,
        J::Real,
        K::Real,
        M::Real,
    )
        mass = mass_au * 1e-3 / NA
        _mass, _dipB = float.(promote(mass, dipB * Debye2SI))
        _J, _K, _M = promote(J, K, M)
        T = typeof(_mass)
        S = typeof(_J)
        return new{T,S}(name, _mass, _dipB, _J, _K, _M)
    end
end

function BasicSymmetricTop1stOrder(
    name::AbstractString,
    mass_au::Real,
    rotB::Real,
    dipB::Real,
    J::Real,
    K::Real,
    M::Real,
)
    return BasicSymmetricTop1stOrder(name, mass_au, dipB, J, K, M)
end


function effective_dipole(particle::BasicSymmetricTop1stOrder, E)
    # Ohoyama et al. J. Phys. Chem., Vol. 99, No. 37, 1995 13607
    μ = particle.dipB
    J = particle.J
    K = particle.K
    M = particle.M

    μeff = ifelse(J > 0, μ * K * M / (J * (J + 1)) * one(E), zero(E))
    return μeff
end


struct SymmetricTopWithInversionSplitting{T<:Real,S<:Real} <: SymmetricTop
    name::String
    mass::T
    dip::T
    Winv::T
    polarity::Bool
    J::S
    K::S
    M::S

    function SymmetricTopWithInversionSplitting(
        name::AbstractString,
        mass_au::Real,
        dip::Real,
        Winv::Real,
        polarity::Bool,
        J::Real,
        K::Real,
        M::Real,
    )
        mass = mass_au * 1e-3 / NA
        _mass, _dip, _Winv = float.(promote(mass, dip * Debye2SI, Winv * wavnum2Hz * h))
        _J, _K, _M = promote(J, K, M)
        T = typeof(_mass)
        S = typeof(_J)
        return new{T,S}(name, _mass, _dip, _Winv, polarity, _J, _K, _M)
    end
end


function effective_dipole(particle::SymmetricTopWithInversionSplitting, E)
    μ = particle.dip
    Winv = particle.Winv
    polarity = particle.polarity
    J = particle.J
    K = particle.K
    M = particle.M

    sign = polarity ? 1 : -1
    ρ = K * M / (J * (J + 1))
    ρ² = ρ^2
    μ² = μ^2
    E² = E^2
    μeff = -sign * μ² * ρ² * E / sqrt((Winv / 2)^2 + μ² * ρ² * E²)
    return μeff
end


end
