module ParticleTrajectorySimulationsCore

export
    AbstractParticle,
    BasicSymmetricTop,
    BasicSymmetricTop1stOrder,
    SymmetricTopWithInversionSplitting,
    mass,
    effective_dipole,
    AbstractApparatus,
    SimpleApparatus,
    Instrument,
    AbstractNozzle,
    IteratorNozzle,
    FunctionNozzle,
    RandomPointNozzle,
    CircularObstacle,
    CircularSlit,
    CircularDetectionPlane,
    RectangularSlit,
    RectangularDetectionPlane,
    Hexapole,
    Deflector,
    number_of_trajectories,
    number_of_recorded_points,
    collect_initial_points,
    collect_end_points,
    collect_success_attribute,
    collect_trajectories

include("progress.jl")
using .Progress:
    CountProgress,
    simulation_next!,
    finish!

include("output.jl")
using .Outputs:
    AbstractOutput,
    HDFOutput,
    initialize_hdf5,
    start_trajectory,
    end_trajectory,
    flush,
    number_of_trajectories,
    number_of_recorded_points,
    collect_initial_points,
    collect_end_points,
    collect_success_attribute,
    collect_trajectories

include("particle.jl")
using .Particles:
    AbstractParticle,
    BasicSymmetricTop,
    BasicSymmetricTop1stOrder,
    SymmetricTopWithInversionSplitting,
    mass,
    effective_dipole

include("apparatus.jl")
using .Apparatuses:
    AbstractApparatus,
    SimpleApparatus,
    Instrument,
    AbstractNozzle,
    IteratorNozzle,
    FunctionNozzle,
    RandomPointNozzle,
    CircularObstacle,
    CircularSlit,
    CircularDetectionPlane,
    RectangularSlit,
    RectangularDetectionPlane,
    pass!,
    emit!

include("electrode.jl")
using .Electrodes:
    Hexapole,
    Deflector,
    efield


# TODO: Simulation 型の実装と HDF5 の構造の変更

function run(
    apparatus::AbstractApparatus,
    particle::AbstractParticle;
    log="starksim.h5",
    show_progress::Bool=true,
    progress_dt::Real=1.0,
    num_threads::Integer=Threads.nthreads(),
)
    available_num_threads = Threads.nthreads()
    if num_threads > available_num_threads
        @error """
            Given num_threads ($(num_threads)) is larger than the number of available threads ($(available_num_threads)).
            Try again with num_threads=$(available_num_threads).
            Or, think of increasing the number of available threads
                by starting julia with the `-t` option, or
                by setting an environmental variable `JULIA_NUM_THREADS`.
            See https://docs.julialang.org/en/v1/manual/multi-threading/
            The current simulation is stopped.
        """
        return
    elseif num_threads < 1
        @error """
            Invalid num_threads ($(num_threads)) less than 1 was given.
            The current simulation is stopped.
        """
        return
    end

    initialize_hdf5(log)
    progress = CountProgress(length(apparatus.nozzle); dt=progress_dt, show=show_progress)
    if num_threads == 1
        run_single_thread(apparatus, particle, log, progress)
    else
        run_multithreads(apparatus, particle, log, progress, num_threads)
    end
    return
end


# This function is free from some overhead of multithreading.
# It would be a little better for light-weight simulations.
# However, actually, the main purpose is for testing and debugging.
function run_single_thread(apparatus, particle, file, progress)
    T = Float64
    row = 7
    col = 1000000
    u = zeros(T, (row,))
    out = HDFOutput{T}(file, (row, col), progress)
    i = 0
    while true
        i += 1
        emit!(u, i, apparatus.nozzle, particle) || break
        run_a_trajectory!(u, i, apparatus, particle, out)
        simulation_next!(progress)
    end
    flush(out)
    finish!(progress)
    return
end


function run_multithreads(apparatus, particle, file, progress, num_threads)
    counter = get_counter()
    countlock = ReentrantLock()
    filelock = ReentrantLock()
    proglock = ReentrantLock()
    tasks = Vector{Task}(undef, num_threads)
    for i in 1:num_threads
        t = Threads.@spawn run_trajectories(
            apparatus,
            particle,
            counter,
            countlock,
            file,
            filelock,
            progress,
            proglock,
        )
        tasks[i] = t
    end
    for t in tasks
        wait(t)
    end
    finish!(progress)
    return
end


function get_counter()
    i = 0
    return function ()
        i += 1
        return i
    end
end


function run_trajectories(
    apparatus,
    particle,
    counter,
    countlock,
    file,
    filelock,
    progress,
    proglock,
)
    T = Float64
    row = 7
    col = 1000000
    u = zeros(T, (row,))
    out = HDFOutput{T}(file, filelock, (row, col), progress)

    while true
        i, emitted = lock(countlock) do
            i = counter()
            emitted = emit!(u, i, apparatus.nozzle, particle)
            return i, emitted
        end
        emitted || break
        run_a_trajectory!(u, i, apparatus, particle, out)
        lock(proglock) do
            simulation_next!(progress)
            return
        end
    end
    flush(out)
    return
end


function run_a_trajectory!(u, i, apparatus, particle, out)
    start_trajectory(out, u, i)
    success = true
    for instrument in apparatus.units
        passed = pass!(u, instrument, particle, out)
        if !passed
            success = false
            break
        end
    end
    end_trajectory(out, success)
    return
end


end
