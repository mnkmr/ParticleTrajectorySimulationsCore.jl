module Outputs

export
    HDFOutput,
    initialize_hdf5,
    start_trajectory,
    end_trajectory,
    record,
    flush,
    number_of_trajectories,
    number_of_recorded_points,
    collect_success_attribute,
    collect_initial_points,
    collect_end_points,
    collect_trajectories

using HDF5:
    HDF5,
    h5open,
    create_group,
    create_dataset,
    read_attribute,
    write_attribute,
    get_extent_dims,
    set_extent_dims

using Base:
    AbstractLock

using ..Progress:
    CountProgress,
    record_next!


abstract type AbstractOutput end


struct DummyOutput <: AbstractOutput end
function start_trajectory(::DummyOutput, u0) end
function end_trajectory(::DummyOutput, success) end
function record(::DummyOutput, u) end


mutable struct HDFOutput{T<:Real,S<:AbstractLock} <: AbstractOutput
    file::String
    filelock::S
    buffer::Matrix{T}
    num_trajectory::Int
    head::Int
    tail::Int
    indices::Vector{Tuple{Int,Int,Int,Union{Bool,Nothing}}}
    progress::CountProgress
end

function HDFOutput{T}(
    file::AbstractString,
    filelock::S,
    shape,
    progress::CountProgress,
) where {T<:Real,S<:AbstractLock}
    buffer = zeros(T, shape)
    num_trajectory = 0
    head = 1
    tail = 0
    return HDFOutput{T,S}(file, filelock, buffer, num_trajectory, head, tail, [], progress)
end

function HDFOutput{T}(file::AbstractString, shape, progress::CountProgress) where {T<:Real}
    filelock = ReentrantLock()
    return HDFOutput{T}(file, filelock, shape, progress)
end


function initialize_hdf5(file)
    h5open(file, "w") do fid
        create_group(fid, "trajectories")
        return
    end
    return
end


function start_trajectory(out::HDFOutput, u0, i)
    out.num_trajectory = i
    record(out, u0)
    return
end


function end_trajectory(out::HDFOutput, success)
    @assert out.num_trajectory > 0
    index = (out.num_trajectory, out.head, out.tail, success)
    push!(out.indices, index)
    out.num_trajectory = 0
    out.head = out.tail + 1
    return
end


const CHUNK_MAXCOL = 10

function record_a_trajectory(
    traj_group::HDF5.Group,
    n::Integer,
    array::AbstractMatrix,
    success::Union{Bool,Nothing},
)
    @assert n > 0
    index = "$(n)"
    if !haskey(traj_group, index)
        T = eltype(array)
        dims = size(array)
        max_dims = (dims[1], -1)
        chunkcol = ifelse(isnothing(success), CHUNK_MAXCOL, min(CHUNK_MAXCOL, dims[2]))
        chunksize = (dims[1], chunkcol)
        d = create_dataset(
            traj_group, index, T, (dims, max_dims);
            chunk=chunksize, shuffle=true, deflate=1,
        )
        d[:, :] = array
    else
        d = traj_group[index]
        shape, _ = get_extent_dims(d)
        oldlen = shape[2]
        newlen = oldlen + size(array, 2)
        new_dims = (size(array, 1), newlen)
        set_extent_dims(d, new_dims)
        d[:, oldlen+1:end] = array
    end

    if !isnothing(success)
        write_attribute(d, "success", success)
    end
    close(d)
    return
end


function flush(out::HDFOutput{T,S}) where {T<:Real,S}
    file = out.file
    lock(out.filelock) do
        h5open(file, "cw") do fid
            if !haskey(fid, "trajectories")
                create_group(fid, "trajectories")
            end
            traj_group = fid["trajectories"]
            _flush(traj_group, out)
            close(traj_group)
            return
        end
    end
    # Following lines does not need lock
    fill!(out.buffer, zero(T))
    out.head = 1
    out.tail = 0
    empty!(out.indices)
    return
end


function _flush(traj_group, out)
    for (n, head, tail, success) in out.indices
        array = view(out.buffer, :, head:tail)
        record_a_trajectory(traj_group, n, array, success)
        record_next!(out.progress)
    end
    if out.tail >= out.head
        n = out.num_trajectory
        head = out.head
        tail = out.tail
        array = view(out.buffer, :, head:tail)
        record_a_trajectory(traj_group, n, array, nothing)
    end
    return
end


function record(out::HDFOutput, u)
    out.tail += 1
    i = out.tail
    out.buffer[:, i] .= u[:]
    if out.tail == size(out.buffer, 2)
        flush(out)
    end
    return
end


## utilities

number_of_row(d::HDF5.Dataset) = HDF5.get_extent_dims(d)[1][1]

"""
    number_of_trajectories(file::HDF5.File)

Return the number of trajectories recorded in an output file.
`file` could be a `HDF5.File` instance.

# Example
```julia
using HDF5
h5open("result.h5", "r") do fid
    number_of_trajectories(fid)
end
```
"""
function number_of_trajectories(fid::HDF5.File)
    i = 0
    while true
        !haskey(fid["trajectories"], "$(i+1)") && break
        i += 1
    end
    return i
end

"""
    number_of_trajectories(file)

Return the number of trajectories recorded in an output file.

# Example
```julia
# Return the number of trajectories recorded in result.h5.
number_of_trajectories("result.h5")
```
"""
function number_of_trajectories(file)
    h5open(file, "r") do fid
        return number_of_trajectories(fid)
    end
end


function number_of_recorded_points(data::HDF5.Dataset)
    return HDF5.get_extent_dims(data)[1][2]
end

"""
    number_of_recorded_points(file::HDF5.File, i)

Return the number of recorded coordinates in a trajectory recorded in an output file.
`file` could be a `HDF5.File` instance.

# Example
```julia
using HDF5
h5open("result.h5", "r") do fid
    number_of_recorded_points(fid)
end
```
"""
function number_of_recorded_points(fid::HDF5.File, i)
    data = fid["trajectories/$(i)"]
    return number_of_recorded_points(data)
end

"""
    number_of_recorded_points(file, i)

Return the number of recorded coordinates in a trajectory recorded in an output file.

# Example
```julia
# Return the number of coordinates in the first trajectory in result.h5.
number_of_recorded_points("result.h5", 1)
```
"""
function number_of_recorded_points(file, i)
    h5open(file, "r") do fid
        return number_of_recorded_points(fid, i)
    end
end


"""
    collect_success_attribute(file::HDF5.File)
    collect_success_attribute(file::HDF5.File, indices)

Return an BitArray representing trajectories succeeded or not.
The `file` could be a HDF5.File instance.

# Example
```julia
h5open("result.h5", "r") do fid
    collect_success_attribute(fid, 1:5)
end
```
"""
function collect_success_attribute(fid::HDF5.File, indices)
    n = length(indices)
    success = BitVector(undef, (n,))
    trajectories = fid["trajectories"]
    for (j, i) in enumerate(indices)
        data = trajectories["$(i)"]
        s = read_attribute(data, "success")
        success[j] = s
        close(data)
    end
    close(trajectories)
    return success
end

function collect_success_attribute(fid::HDF5.File)
    n = number_of_trajectories(fid)
    return collect_success_attribute(fid, 1:n)
end

"""
    collect_success_attribute(file)
    collect_success_attribute(file, indices)

Return an BitArray representing trajectories succeeded or not.
The returned BitArray corresponds to a part of the trajectories specified by `indices` if it is given.

# Example
```julia
# Return succeeded/failed info of all the trajectories in result.h5.
collect_success_attribute("result.h5")

# Return succeeded/failed info of the first five trajectories in result.h5.
collect_success_attribute("result.h5", 1:5)
```
"""
function collect_success_attribute(file, indices)
    h5open(file, "r") do fid
        return collect_success_attribute(fid, indices)
    end
end

function collect_success_attribute(file)
    h5open(file, "r") do fid
        return collect_success_attribute(fid)
    end
end


"""
    collect_initial_points(file::HDF5.File)
    collect_initial_points(file::HDF5.File, indices)

Return the initial points of the trajectories as an Array.
`file` could be a `HDF5.File` instance.

# Example
```julia
# Return the initial points of the succeeded and failed trajectories in result.h5.
h5open("result.h5", "r") do fid
    success = collect_success_attribute(fid)
    succeeded = collect_initial_points(fid, success)
    failed = collect_initial_points(fid, .!success)
end
```
"""
function collect_initial_points(fid::HDF5.File, indices)
    traj_group = fid["trajectories"]
    row = number_of_row(traj_group["1"])
    col = length(indices)
    points = Array{Float64}(undef, (row, col))
    for (j, i) in enumerate(indices)
        data = traj_group["$(i)"]
        points[:, j] .= data[:, 1]
        close(data)
    end
    close(traj_group)
    return points
end

function collect_initial_points(fid::HDF5.File, bitvec::BitVector)
    n = number_of_trajectories(fid)
    indices = [i for i in 1:n if bitvec[i]]
    return collect_initial_points(fid, indices)
end

function collect_initial_points(fid::HDF5.File)
    n = number_of_trajectories(fid)
    return collect_initial_points(fid, 1:n)
end

"""
    collect_initial_points(file)
    collect_initial_points(file, indices)

Return the initial points of the trajectories as an Array.
The returend array is *7-by-n* sized; the *n* is the number of trajectories retrieved from the `file`.
The column of the array corresponds to an initial point of a trajectory, and it is `[time, x, y, z, vx, vy, vz]`.
The returned array corresponds to a part of the trajectories specified by `indices` if it is given.

# Example
```julia
# Return the initial points of all the trajectories in result.h5.
collect_initial_points("result.h5")

# Return the initial points of the first five trajectories in result.h5.
collect_initial_points("result.h5", 1:5)

# Return the initial points of the succeeded and failed trajectories in result.h5.
success = collect_success_attribute("result.h5")
succeeded = collect_initial_points("result.h5", success)
failed = collect_initial_points("result.h5", .!success)
```
"""
function collect_initial_points(file, indices)
    h5open(file, "r") do fid
        return collect_initial_points(fid, indices)
    end
end

function collect_initial_points(file)
    h5open(file, "r") do fid
        return collect_initial_points(fid)
    end
end


"""
    collect_end_points(file::HDF5.File)
    collect_end_points(file::HDF5.File, indices)

Return the last points of the trajectories as an Array.
`file` could be a `HDF5.File` instance.

# Example
```julia
# Return the last points of the succeeded and failed trajectories in result.h5.
h5open("result.h5", "r") do fid
    success = collect_success_attribute(fid)
    succeeded = collect_end_points(fid, success)
    failed = collect_end_points(fid, .!success)
end
```
"""
function collect_end_points(fid::HDF5.File, indices)
    traj_group = fid["trajectories"]
    row = number_of_row(traj_group["1"])
    col = length(indices)
    points = Array{Float64}(undef, (row, col))
    for (j, i) in enumerate(indices)
        data = traj_group["$(i)"]
        points[:, j] .= data[:, end]
        close(data)
    end
    close(traj_group)
    return points
end

function collect_end_points(fid::HDF5.File, bitvec::BitVector)
    n = number_of_trajectories(fid)
    indices = [i for i in 1:n if bitvec[i]]
    return collect_end_points(fid, indices)
end

function collect_end_points(fid::HDF5.File)
    n = number_of_trajectories(fid)
    return collect_end_points(fid, 1:n)
end

"""
    collect_end_points(file)
    collect_end_points(file, indices)

Return the last points of the trajectories as an Array.
The returend array is *7-by-n* sized; the *n* is the number of trajectories retrieved from the `file`.
The column of the array corresponds to a last point of a trajectory, and it is `[time, x, y, z, vx, vy, vz]`.
The returned array corresponds to a part of the trajectories specified by `indices` if it is given.

# Example
```julia
# Return the last points of all the trajectories in result.h5.
collect_end_points("result.h5")

# Return the last points of the first five trajectories in result.h5.
collect_end_points("result.h5", 1:5)

# Return the last points of the succeeded and failed trajectories in result.h5.
success = collect_success_attribute("result.h5")
succeeded = collect_end_points("result.h5", success)
failed = collect_end_points("result.h5", .!success)
```
"""
function collect_end_points(file, indices)
    h5open(file, "r") do fid
        return collect_end_points(fid, indices)
    end
end

function collect_end_points(file)
    h5open(file, "r") do fid
        return collect_end_points(fid)
    end
end


"""
    collect_trajectories(file::HDF5.File)
    collect_trajectories(file::HDF5.File, indices)

Return trajectory data as a Vector of Matrices.
`file` could be a `HDF5.File` instance.

# Example
```julia
# Return the succeeded and failed trajectories in result.h5.
h5open("result.h5", "r") do fid
    success = collect_success_attribute(fid)
    succeeded = collect_end_points(fid, success)
    failed = collect_end_points(fid, .!success)
end
```
"""
function collect_trajectories(fid::HDF5.File, indices)
    traj_group = fid["trajectories"]
    row = number_of_row(traj_group["1"])
    n = length(indices)
    trajectories = Vector{Matrix{Float64}}(undef, (n,))
    for (j, i) in enumerate(indices)
        data = traj_group["$(i)"]
        m = number_of_recorded_points(data)
        traj = zeros(Float64, (row, m))
        traj[:, :] .= data[:, :]
        trajectories[j] = traj
        close(data)
    end
    close(traj_group)
    return trajectories
end

function collect_trajectories(fid::HDF5.File, bitvec::BitVector)
    n = number_of_trajectories(fid)
    indices = [i for i in 1:n if bitvec[i]]
    return collect_trajectories(fid, indices)
end

function collect_trajectories(fid::HDF5.File)
    n = number_of_trajectories(fid)
    return collect_trajectories(fid, 1:n)
end

"""
    collect_trajectories(file)
    collect_trajectories(file, indices)

Return trajectory data as a Vector of Matrices.
Each item in the returned Vector is a *7-by-m* Matrix, where the *m* is the number of recorded coordinates of the trajectory.
The column of the array corresponds to `[time, x, y, z, vx, vy, vz]`.
The returned trajectories is a part of all trajectories specified by `indices` if it is given.

# Example
```julia
# Return all the trajectories in result.h5.
collect_end_points("result.h5")

# Return the first five trajectories in result.h5.
collect_end_points("result.h5", 1:5)

# Return the succeeded and failed trajectories in result.h5.
success = collect_success_attribute("result.h5")
succeeded = collect_end_points("result.h5", success)
failed = collect_end_points("result.h5", .!success)
```
"""
function collect_trajectories(file, indices)
    h5open(file, "r") do fid
        return collect_trajectories(fid, indices)
    end
end

function collect_trajectories(file)
    h5open(file, "r") do fid
        return collect_trajectories(fid)
    end
end


end
