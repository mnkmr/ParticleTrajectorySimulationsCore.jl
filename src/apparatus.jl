module Apparatuses

using Distributions: UnivariateDistribution, Uniform
using LinearAlgebra: dot, cross, norm
using Printf: @printf
using Random: AbstractRNG, default_rng, rand
using Rotations: Rotation, RotZYZ
using StaticArrays: SVector

using ..Outputs:
    AbstractOutput,
    record

using ..Particles:
    AbstractParticle

const Vector3D = Union{AbstractArray,NTuple{3,Number}}

# Note
# * The coordinates are expressed in (x, y, z) notation, not in (y, x, z)
# * The parts of apparatus should not spatially overlapp.

abstract type Object end


## Shapes

abstract type AbstractPlane{T<:Real} <: Object end


function relative_system(u, obj::Object)
    q = obj.orientation
    r = SVector(u[2], u[3], u[4])
    r′ = q \ (r .- obj.coordinates)
    v = SVector(u[5], u[6], u[7])
    v′ = q \ v
    return SVector(u[1], r′[1], r′[2], r′[3], v′[1], v′[2], v′[3])
end


function original_system(u, obj::Object)
    q = obj.orientation
    r′ = SVector(u[2], u[3], u[4])
    r = q * r′ .+ obj.coordinates
    v′ = SVector(u[5], u[6], u[7])
    v = q * v′
    return SVector(u[1], r[1], r[2], r[3], v[1], v[2], v[3])
end


function _crossed_at(abstplane::AbstractPlane, u)
    u0 = relative_system(u, abstplane)
    z0 = u0[4]
    # sqrt(eps(T)) is about an acurracy limit of T.
    # FIXME: No reason to double the limit as the threshold, what number could be reasonable?
    thr = 2 * eps(float(typeof(u0[7])))
    vz = abs(u0[7]) > thr ? u0[7] : zero(eltype(u0))
    Δt = -z0 / vz
    !isfinite(Δt) && return false, nothing
    Δt < 0 && return false, nothing
    t0 = u0[1]
    x0 = u0[2]
    y0 = u0[3]
    vx = u0[5]
    vy = u0[6]
    tc = t0 + Δt
    xc = x0 + vx * Δt
    yc = y0 + vy * Δt
    zc = z0 + vz * Δt
    uc = original_system((tc, xc, yc, zc, vx, vy, vz), abstplane)
    return true, uc
end

"""
    crossed, c = crossed_at(abstplane::AbstractPlane, coord, velocity)

Judge whether a moving point collides with the plane.
`crossed` is a Boolean, true if the point collides with the plane.
`c` are the coordinates at which the point crossed.
`c` would be the coordinates that the point collides with the infinite plane
including the `abstplane` even if `crossed` is false.
`c` would be `nothing` if the point never collides with the infinite plane.
"""
function crossed_at end


"""
    Plane{T<:Real} <: AbstractPlane{T}

A 2D plane object in 3D.
"""
struct Plane{T<:Real,S<:Rotation} <: AbstractPlane{T}
    # a point included in the plane.
    coordinates::SVector{3,T}

    # an instance of a subtype of Rotation that orients [0, 0, 1]
    # vector to the normal vector of the plane.
    orientation::S

    function Plane(coordinates::SVector{3,T}, orientation::S) where {T<:Real,S<:Rotation}
        if !all(isfinite, coordinates)
            throw(
                DomainError(
                    coordinates,
                    "All items in coordinates should be finite numbers.",
                ),
            )
        end
        return new{T,S}(coordinates, orientation)
    end
end

function Plane(coordinates::Vector3D, orientation::Rotation)
    if length(coordinates) != 3
        throw(DomainError(coordinates, "The length of \"coordinates\" should be 3."))
    end
    coordinates_T = float.(coordinates)
    coordinates_SVector = SVector(coordinates_T[1], coordinates_T[2], coordinates_T[3])
    return Plane(coordinates_SVector, orientation)
end

"""
    Plane(coordinates, orientation)

`coordinates` is an iterator having 3 items corresponding to a point
[xc, yc, zc] in the plane.

`orientation` is an iterator having 3 items corresponding to Euler angles in
ZYZ convention.

A plane is created including the origin [0, 0, 0] with its normal vector is
oriented toward z direction [0, 0, 1].
The plane is then rotated with the normal vector by the `orientation`
and moved in parallel to include `coordinates` in the plane.
"""
function Plane(coordinates::Vector3D, orientation::Vector3D)
    if length(orientation) != 3
        throw(DomainError(orientation, "The length of \"orientation\" should be 3."))
    end
    orientation_RotZYZ = RotZYZ(orientation[1], orientation[2], orientation[3])
    return Plane(coordinates, orientation_RotZYZ)
end

Plane(apl::AbstractPlane) = Plane(apl.coordinates, apl.orientation)


crossed_at(plane, u) = _crossed_at(plane, u)


"""
    Circle{T<:Real,S<:Rotation} <: AbstractPlane{T}

A circular object in 3D space.
"""
struct Circle{T<:Real,S<:Rotation} <: AbstractPlane{T}
    # the coordinates of the center of the circle.
    coordinates::SVector{3,T}

    # an Rotation instance that orient [0, 0, 1] vector to the
    # normal vector of the circle.
    orientation::S

    # the radius of the circle.
    radius::T

    function Circle(
        coordinates::SVector{3,T},
        orientation::S,
        radius::T) where {T<:Real,S<:Rotation}
        if !all(isfinite, coordinates)
            throw(
                DomainError(
                    coordinates,
                    "All items in coordinates should be finite numbers.",
                ),
            )
        end
        return new{T,S}(coordinates, orientation, radius)
    end
end

function Circle(coordinates::Vector3D, orientation::Rotation, radius::Real)
    if length(coordinates) != 3
        throw(DomainError(coordinates, "The length of \"coordinates\" should be 3."))
    end
    radius_T, coordinates_T... = float.(promote(radius, coordinates...))
    coordinates_SVector = SVector(coordinates_T[1], coordinates_T[2], coordinates_T[3])
    return Circle(coordinates_SVector, orientation, radius_T)
end

"""
    Circle(coordinates, orientation, radius)

`coordinates` is an iterator that have 3 items corresponding to the center
[xc, yc, zc] of the circle.

`orientation` is an iterator having 3 items corresponding to Euler angles in
ZYZ convention.

A circle is created with its normal vector oriented toward z direction
[0, 0, 1], and with its center located at the origin [0, 0, 0].
The circlar plane is then rotated with the normal vector by the `orientation`
and translated to match its center with `coordinates`.
"""
function Circle(coordinates::Vector3D, orientation::Vector3D, radius::Real)
    if length(orientation) != 3
        throw(DomainError(orientation, "The length of \"orientation\" should be 3."))
    end
    orientation_RotZYZ = RotZYZ(orientation[1], orientation[2], orientation[3])
    return Circle(coordinates, orientation_RotZYZ, radius)
end


function is_in_sphere(coord, origin, r)
    x, y, z = coord
    x0, y0, z0 = origin
    return (x - x0)^2 + (y - y0)^2 + (z - z0)^2 ≤ r^2
end


function crossed_at(circ::Circle, u)
    crossed, uc = _crossed_at(circ, u)
    !crossed && return false, uc
    c = uc[2:4]
    isinside = is_in_sphere(c, circ.coordinates, circ.radius)
    return isinside, uc
end


"""
    Rectangle{T<:Real} <: AbstractPlane{T}

A rectangular object in 3D space.
"""
struct Rectangle{T<:Real,S<:Rotation} <: AbstractPlane{T}
    coordinates::SVector{3,T}
    orientation::S
    width::T
    height::T

    function Rectangle(
        coordinates::SVector{3,T},
        orientation::S,
        width::T,
        height::T,
    ) where {T<:Real,S<:Rotation}
        if !all(isfinite, coordinates)
            throw(
                DomainError(
                    coordinates,
                    "All items in coordinates should be finite numbers.",
                ),
            )
        end
        return new{T,S}(coordinates, orientation, width, height)
    end
end

function Rectangle(coordinates::Vector3D, orientation::Rotation, width::Real, height::Real)
    if length(coordinates) != 3
        throw(DomainError(coordinates, "The length of \"coordinates\" should be 3."))
    end
    width_T, height_T, coordinates_T... = float.(promote(width, height, coordinates...))
    coordinates_SVector = SVector(coordinates_T[1], coordinates_T[2], coordinates_T[3])
    return Rectangle(coordinates_SVector, orientation, width_T, height_T)
end

"""
    Rectangle(coordinates, orientation, width, height)

`coordinates` is an iterator having 3 items corresponding to the center
[xc, yc, zc] of the rectangle.

`orientation` is an iterator having 3 items corresponding to Euler angles in
ZYZ convention.

A rectangle is created with its normal vector oriented toward z direction
[0, 0, 1], and with its center located at the origin [0, 0, 0].
The `width` is the length along the x-axis and the `height` is that along y-axis.
The rectangular shape is then rotated θ radian around z-axis in clockwise.
Finally, the rectangular shape is rotated with the normal vector by the
`orientation` and translated to match its center with `coordinates`.
"""
function Rectangle(coordinates::Vector3D, orientation::Vector3D, width::Real, height::Real)
    if length(orientation) != 3
        throw(DomainError(orientation, "The length of \"orientation\" should be 3."))
    end
    orientation_RotZYZ = RotZYZ(orientation[1], orientation[2], orientation[3])
    return Rectangle(coordinates, orientation_RotZYZ, width, height)
end


function crossed_at(rect::Rectangle, u)
    crossed, uc = _crossed_at(rect, u)
    !crossed && return false, uc
    uc′ = relative_system(uc, rect)
    abs(uc′[2]) > rect.width / 2 && return false, uc
    abs(uc′[3]) > rect.height / 2 && return false, uc
    return true, uc
end


## Instruments

abstract type Instrument <: Object end
abstract type AbstractNozzle <: Instrument end

# Nozzle should be implemented with `emit!` and `Base.length`
#
# continuing = emit!(u, i, nozzle::AbstractNozzle, particle::AbstractParticle)
#   continuing:bool: Continue simulation if true, otherwise stop ignoring the current u
#   u:Array{T}: [time, x, y, z, vx, vy, vz]
#   i:Int: 1-based index of particle
#   nozzle:AbstractNozzle: A nozzle object -> apparatus.jl
#   particle:AbstractParticle: A particle object -> particle.jl
#
# len = length(nozzle::AbstractNozzle)
#   len:Union{Int,Nothing}: the number of particles to simulate

function emit! end
function pass! end


function Base.length(nozzle::AbstractNozzle)
    return nothing
end


"""
    IteratorNozzle{T}

A nozzle parametrized by an iterable.
"""
struct IteratorNozzle{T,S} <: AbstractNozzle
    iter::T
    axis::S

    """
        IteratorNozzle(iter::T) <: AbstractNozzle

    A constructor of the IterableNozzele type. `iter` should be an iterable and
    its item should be an initial time (1 item), an initial coordinates (3 items),
    and a initial velocity (3 items) of a particle in [t, x, y, z, vx, vy, vz] manner.
    An item could be a Tuple, Vector, or something iterable with the 7 items.

    ```julia
    using ParticleTrajectorySimulationsCore

    # Fix the initial time at zero
    t0 = 0.0
    # Fix the initial position at the origin
    x0, y0, z0 = 0.0, 0.0, 0.0
    # Norm of the velocity vector [m/s]
    v0 = 500.0

    iter = [[t0, x0, y0, z0, 0.0, v0*sind(θ), v0*cosd(θ)] for θ in -5:0.1:5]
    nozzle = IteratorNozzle(iter)
    ```

    See also: [`zip`](@ref)
    """
    function IteratorNozzle(iter::T) where {T}
        if length(methods(iterate, (T,))) < 1
            throw(DomainError("The \"iter\" should be iterable."))
        end
        axs = axes(iter)
        length(axs) > 1 && error("The iterable should be one-dimensional.")
        ax = first(axs)
        S = typeof(ax)
        return new{T,S}(deepcopy(iter), ax)
    end
end


function emit!(u, i, nozzle::IteratorNozzle, particle::AbstractParticle)
    i > length(nozzle.axis) && return false
    j = nozzle.axis[i]
    u .= nozzle.iter[j]
    return true
end


function Base.length(nozzle::IteratorNozzle)
    return length(nozzle.iter)
end


"""
    FunctionNozzle{F}

A nozzle parametrized by an callable object.
"""
struct FunctionNozzle{F} <: AbstractNozzle
    f!::F

    """
        FunctionNozzle(f!::F) <: AbstractNozzle

    A constructor of the FunctionNozzle type. `f!` is a callable object that
    takes 3 arguments, (u, i, particle). `u` is a [`Vector`](@ref) instance
    with 7 items which are an initial time (1 item), an initial coordinates
    (3 items), and an initial veclocity (3 items) of a particle in
    [t, x, y, z, vx, vy, vz] manner. `f!` should directly update the `u`.
    `i` is the number of trajectry for the emitting particle, which begins with
    1 and will be incremented for the followings.
    `particle` is an instance of an [`AbstractParticle`](@ref).
    `f!` should return a boolean value; the simulation is continued if `true`
    otherwise exited.

    ```julia
    using ParticleTrajectorySimulationsCore

    function nozzlefunc!(u, i, particle)
        n = 500
        i > n && return false

        # Fix the initial time at zero
        t0 = 0.0
        # Fix the initial position at the origin
        x0, y0, z0 = 0.0, 0.0, 0.0
        # Norm of the velocity vector [m/s]
        v0 = 500.0

        θi = -5
        θf =  5
        θ = θi + (i - 1)*(θf - θi)/n
        u[1] = t0
        u[2] = x0
        u[3] = y0
        u[4] = z0
        u[5] = 0.0
        u[6] = v0*sind(θ)
        u[7] = v0*cosd(θ)
        return true
    end
    nozzle = FunctionNozzle(iter)
    ```
    """
    function FunctionNozzle(f)
        !iscallable(f) && throw(DomainError(f, "The argument \"f\" should be callable."))
        return new{typeof(f)}(f)
    end
end


# https://discourse.julialang.org/t/how-to-determine-if-an-object-is-callable/2897
iscallable(f) = !isempty(methods(f))


function emit!(u, i, nozzle::FunctionNozzle, particle::AbstractParticle)
    continuing = nozzle.f!(u, i, particle)
    return continuing
end


function Base.length(nozzle::FunctionNozzle)
    return nothing
end


"""
    RandomPointNozzle{T<:Real,
                      D<:UnivariateDistribution,
                      R<:AbstractRNG} <: AbstractNozzle

A nozzle fixed at a point. It emits particles randomly from the initial point
into a given solid angle.
"""
struct RandomPointNozzle{
    T<:Real,
    S<:Rotation,
    D<:UnivariateDistribution,
    R<:AbstractRNG,
} <: AbstractNozzle
    coordinates::SVector{3,T}
    orientation::S
    n::Int
    vdist::D
    cosθdist::Uniform
    rng::R
end

function RandomPointNozzle(
    coordinates::SVector{3,T},
    orientation::S,
    n::Integer,
    vdist::D,
    theta::Real,
    rng::R,
) where {T<:Real,S<:Rotation,D<:UnivariateDistribution,R<:AbstractRNG}
    cosθdist = Uniform(cosd(theta), one(theta))
    return RandomPointNozzle{T,S,D,R}(coordinates, orientation, n, vdist, cosθdist, rng)
end

function RandomPointNozzle(
    coordinates::Vector3D,
    orientation::Rotation,
    n::Integer,
    vdist::UnivariateDistribution,
    theta::Real,
    rng::AbstractRNG,
)
    if length(coordinates) != 3
        throw(DomainError(coordinates, "The length of \"coordinates\" should be 3."))
    end
    coordinates_T = float.(coordinates)
    coordinates_SVector = SVector(coordinates_T[1], coordinates_T[2], coordinates_T[3])
    return RandomPointNozzle(coordinates_SVector, orientation, n, vdist, theta, rng)
end

function RandomPointNozzle(
    coordinates::Vector3D,
    orientation::Vector3D,
    n::Integer,
    vdist::UnivariateDistribution,
    theta::Real,
    rng::AbstractRNG,
)
    if length(orientation) != 3
        throw(DomainError(orientation, "The length of \"orientation\" should be 3."))
    end
    orientation_RotZYZ = RotZYZ(orientation[1], orientation[2], orientation[3])
    return RandomPointNozzle(coordinates, orientation_RotZYZ, n, vdist, theta, rng)
end

"""
    RandomPointNozzle(coordinates, orientation, n::Integer,
                      vdist::UnivariateDistribution, theta::Real;
                      rng::AbstractRNG=default_rng())

A constructor of the RandomPointNozzele type.

`coordinates` is an iterator that have 3 items corresponding to the initial
coordinates [x0, y0, z0] of emitting particles.

`orientation` is an iterator having 3 items corresponding to Euler angles in
ZYZ convention.

`n` is the number of particle to emit.

`vdist` is an distribution to sample particle velocities.

`theta` is a half apex angle of cone to emit partcles.
"""
function RandomPointNozzle(
    coordinates::Vector3D,
    orientation,
    n::Integer,
    vdist::UnivariateDistribution,
    theta::Real;
    rng::AbstractRNG=default_rng(),
)
    return RandomPointNozzle(coordinates, orientation, n, vdist, theta, rng)
end

function RandomPointNozzle(
    coordinates::SVector{3,T},
    orientation::Rotation,
    n::Integer,
    vmin::Real,
    vmax::Real, theta::Real, rng::AbstractRNG,
) where {T<:Real}
    !isfinite(vmin) && throw(DomainError(vmin, "Vmin should be a finite scalar."))
    !isfinite(vmax) && throw(DomainError(vmax, "Vmax should be a finite scalar."))
    vmin > vmax && throw(DomainError((vmin, vmax), "Vmin should be smaller than vmax."))
    vdist = Uniform(vmin, vmax)
    return RandomPointNozzle(coordinates, orientation, n, vdist, theta, rng)
end

function RandomPointNozzle(
    coordinates::Vector3D,
    orientation::Rotation,
    n::Integer,
    vmin::Real,
    vmax::Real,
    theta::Real,
    rng::AbstractRNG,
)
    if length(coordinates) != 3
        throw(DomainError(coordinates, "The length of \"coordinates\" should be 3."))
    end
    coordinates_T = float.(coordinates)
    coordinates_SVector = SVector(coordinates_T[1], coordinates_T[2], coordinates_T[3])
    return RandomPointNozzle(coordinates_SVector, orientation, n, vmin, vmax, theta, rng)
end

function RandomPointNozzle(
    coordinates::Vector3D,
    orientation::Vector3D,
    n::Integer,
    vmin::Real,
    vmax::Real,
    theta::Real,
    rng::AbstractRNG,
)
    if length(orientation) != 3
        throw(DomainError(orientation, "The length of \"orientation\" should be 3."))
    end
    orientation_RotZYZ = RotZYZ(orientation[1], orientation[2], orientation[3])
    return RandomPointNozzle(coordinates, orientation_RotZYZ, n, vmin, vmax, theta, rng)
end

"""
    RandomPointNozzle(coordinates, n::Integer, vmin::Real,
                      vmax::Real, theta::Real; rng=default_rng())

    RandomPointNozzle(coordinates, orientation, n::Integer, vmin::Real,
                      vmax::Real, theta::Real; rng=default_rng())

A constructor of the RandomPointNozzele type.
Initial velocities are sampled between `vmin` and `vmax` uniformly.
`orientation` will be (0, 0, 0) if omitted.
"""
function RandomPointNozzle(
    coordinates::Vector3D,
    orientation,
    n::Integer,
    vmin::Real,
    vmax::Real,
    theta::Real;
    rng::AbstractRNG=default_rng(),
)
    return RandomPointNozzle(coordinates, orientation, n, vmin, vmax, theta, rng)
end

function RandomPointNozzle(
    coordinates::Vector3D,
    n::Integer,
    vmin::Real,
    vmax::Real,
    theta::Real;
    rng::AbstractRNG=default_rng(),
)
    return RandomPointNozzle(coordinates, (0, 0, 0), n, vmin, vmax, theta, rng)
end


function emit!(
    u,
    i,
    nozzle::RandomPointNozzle{T,S,D,R},
    particle::AbstractParticle,
) where {T,S,D,R}
    i > nozzle.n && return false
    u[1] = zero(u[1])
    u[2:4] .= nozzle.coordinates
    v0 = rand(nozzle.rng, nozzle.vdist)
    cosθ = rand(nozzle.rng, nozzle.cosθdist)
    sinθ = sqrt(1 - cosθ^2)
    ϕ = 2π * rand(nozzle.rng, T)
    q = nozzle.orientation
    v = SVector(v0 * sinθ * cos(ϕ), v0 * sinθ * sin(ϕ), v0 * cosθ)
    u[5:7] .= q * v
    return true
end


function Base.length(nozzle::RandomPointNozzle)
    return nozzle.n
end


"""
    CircularObstacle{T<:Real} <: Instrument

A circular object that blocks particles which collides with.
"""
struct CircularObstacle{T<:Real,S<:Rotation} <: Instrument
    body::Circle{T,S}
end

"""
    CircularObstacle(coordinates, radius)
    CircularObstacle(coordinates, orientation, radius)

A simple circular obstacle which blocks particles colliding inside the circular
region with `radius`.

`coordinates` is an iterator that have 3 items corresponding to the center
[xc, yc, zc] of the circle.

`orientation` is an iterator having 3 items corresponding to Euler angles in
ZYZ convention. It will be (0, 0, 0) if omitted.

A circular obstacle is created with its normal vector oriented toward z
direction [0, 0, 1], and with its center located at the origin [0, 0, 0].
The obstacle is then rotated with the normal vector by the `orientation`
and translated to match the center with `coordinates`.
"""
function CircularObstacle(coordinates::Vector3D, orientation, radius::Real)
    obstacle = Circle(coordinates, orientation, radius)
    return CircularObstacle(obstacle)
end

function CircularObstacle(coordinates::Vector3D, radius::Real)
    obstacle = Circle(coordinates, (0, 0, 0), radius)
    return CircularObstacle(obstacle)
end


function pass!(u, circ_obstcl::CircularObstacle, p::AbstractParticle, out::AbstractOutput)
    crossed_obstacle, uc_obstacle = crossed_at(circ_obstcl.body, u)
    if crossed_obstacle
        u .= uc_obstacle
        record(out, u)
        return false
    end

    pl = Plane(circ_obstcl.body)
    crossed_plane, uc_plane = crossed_at(pl, u)
    !crossed_plane && return true
    isnothing(uc_plane) && return true
    u .= uc_plane
    record(out, u)
    return true
end


"""
    CircularSlit{T<:Real} <: Instrument

A plane object that blocks particles only except passing through the circular
hole.
"""
struct CircularSlit{T<:Real} <: Instrument
    hole::Circle{T}
end

"""
    CircularSlit(coordinates, radius)
    CircularSlit(coordinates, orientation, radius)

CircularSlit is a simple circular slit which passes through particles going
inside the circular region with `radius`.

`coordinates` is an iterator that have 3 items corresponding to the center
[xc, yc, zc] of the circle.

`orientation` is an iterator having 3 items corresponding to Euler angles in
ZYZ convention. It will be (0, 0 ,0) if omitted.

A circular slit is created with its normal vector oriented toward z direction
[0, 0, 1], and with its center located at the origin [0, 0, 0].
The slit is then rotated with the normal vector by the `orientation`
and translated to match the center with `coordinates`.
"""
function CircularSlit(coordinates::Vector3D, orientation, radius::Real)
    hole = Circle(coordinates, orientation, radius)
    return CircularSlit(hole)
end

function CircularSlit(coordinates::Vector3D, radius::Real)
    hole = Circle(coordinates, (0, 0, 0), radius)
    return CircularSlit(hole)
end


function pass!(u, circ_slit::CircularSlit, p::AbstractParticle, out::AbstractOutput)
    crossed, uc = crossed_at(circ_slit.hole, u)
    if !isnothing(uc)
        u .= uc
        record(out, u)
    end
    return crossed
end


struct CircularDetectionPlane{T<:Real} <: Instrument
    sense::Circle{T}
end

function CircularDetectionPlane(coordinates::Vector3D, orientation, radius::Real)
    sense = Circle(coordinates, orientation, radius)
    return CircularDetectionPlane(sense)
end

function CircularDetectionPlane(coordinates::Vector3D, radius::Real)
    sense = Circle(coordinates, (0, 0, 0), radius)
    return CircularDetectionPlane(sense)
end


function pass!(
    u,
    det_plane::CircularDetectionPlane,
    p::AbstractParticle,
    out::AbstractOutput,
)
    crossed, uc = crossed_at(det_plane.sense, u)
    if !isnothing(uc)
        u .= uc
        record(out, u)
    end
    return crossed
end


"""
    RectangularSlit{T<:Real} <: Instrument

A plane object that blocks particles only except passing through the
rectangular hole.
"""
struct RectangularSlit{T<:Real} <: Instrument
    hole::Rectangle{T}
end

"""
    RectangularSlit(coordinates, width, height)
    RectangularSlit(coordinates, orientation, width, height)

RectangularSlit is a simple rectangular slit which passes through particles
going inside the rectangular region.

`coordinates` is an iterator that have 3 items corresponding to the center
[xc, yc, zc] of the circle.

`orientation` is an iterator having 3 items corresponding to Euler angles in
ZYZ convention. It will be (0, 0, 0) if omitted.

A rectangular slit is created with its normal vector oriented toward z
direction [0, 0, 1], and with its center located at the origin [0, 0, 0].
The `width` is the length along the x-axis and the `height` is that along y-axis.
The rectangular slit is then rotated θ radian around z-axis in clockwise.
Finally, the slit is rotated with the normal vector by the `orientation`
and translated to match the center with `coordinates`.
"""
function RectangularSlit(coordinates::Vector3D, orientation, width::Real, height::Real)
    hole = Rectangle(coordinates, orientation, width, height)
    return RectangularSlit(hole)
end

function RectangularSlit(coordinates::Vector3D, width::Real, height::Real)
    hole = Rectangle(coordinates, (0, 0, 0), width, height)
    return RectangularSlit(hole)
end


function pass!(u, rec_slit::RectangularSlit, p::AbstractParticle, out::AbstractOutput)
    crossed, uc = crossed_at(rec_slit.hole, u)
    if !isnothing(uc)
        u .= uc
        record(out, u)
    end
    return crossed
end


struct RectangularDetectionPlane{T<:Real} <: Instrument
    sense::Rectangle{T}
end

function RectangularDetectionPlane(coordinates::Vector3D, width::Real, height::Real)
    rect = Rectangle(coordinates, (0, 0, 0), width, height)
    return RectangularDetectionPlane(rect)
end

function RectangularDetectionPlane(
    coordinates::Vector3D,
    orientation,
    width::Real,
    height::Real,
)
    rect = Rectangle(coordinates, orientation, width, height)
    return RectangularDetectionPlane(rect)
end


function pass!(
    u,
    det_plane::RectangularDetectionPlane,
    p::AbstractParticle,
    out::AbstractOutput,
)
    crossed, uc = crossed_at(det_plane.sense, u)
    if !isnothing(uc)
        u .= uc
        record(out, u)
    end
    return crossed
end


abstract type AbstractApparatus end


struct SimpleApparatus <: AbstractApparatus
    nozzle::AbstractNozzle
    units::Vector{Instrument}
end


function Base.show(io::IO, ::MIME"text/plain", z::SimpleApparatus)
    n = length(z.units)
    plural = ifelse(n == 1, "", "s")
    @printf(io, "SimpleApparatus with %d part%s\n", n, plural)
end


end
