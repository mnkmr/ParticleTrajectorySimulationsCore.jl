using Test
using ParticleTrajectorySimulationsCore
using ParticleTrajectorySimulationsCore.Outputs:
    HDFOutput,
    start_trajectory,
    end_trajectory,
    record,
    flush
using HDF5:
    h5open

ROW = 7
OUTFILE = tempname()
LK = Base.Threads.ReentrantLock()
PROG = ParticleTrajectorySimulationsCore.CountProgress(nothing, show=false)
function temporaryfile(f)
    f(OUTFILE)
    rm(OUTFILE)
end

@testset "HDFOutput" begin
    temporaryfile() do file
        col = 1
        out = HDFOutput{Float64}(file, LK, (ROW, col), PROG)

        u0 = [0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0]
        start_trajectory(out, u0, 1)
        u1 = [0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0]
        record(out, u1)
        end_trajectory(out, nothing)
        flush(out)

        h5open(file, "r") do fid
            @test haskey(fid, "trajectories/1")
            d = fid["trajectories/1"]
            @test size(d) == (7, 2)
            @test d[:, 1] == u0
            @test d[:, 2] == u1
        end
    end

    temporaryfile() do file
        col = 5
        out = HDFOutput{Float64}(file, LK, (ROW, col), PROG)

        u0 = [0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0]
        start_trajectory(out, u0, 1)
        u1 = [0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0]
        record(out, u1)
        end_trajectory(out, nothing)
        flush(out)

        h5open(file, "r") do fid
            @test haskey(fid, "trajectories/1")
            d = fid["trajectories/1"]
            @test size(d) == (7, 2)
            @test d[:, 1] == u0
            @test d[:, 2] == u1
        end
    end

    temporaryfile() do file
        col = 5
        out = HDFOutput{Float64}(file, LK, (ROW, col), PROG)

        u0 = [0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0]
        start_trajectory(out, u0, 1)
        u1 = [0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0]
        record(out, u1)
        u2 = [0.0, 2.0, 0.0, 0.0, 1.0, 0.0, 0.0]
        record(out, u2)
        u3 = [0.0, 3.0, 0.0, 0.0, 1.0, 0.0, 0.0]
        record(out, u3)
        u4 = [0.0, 4.0, 0.0, 0.0, 1.0, 0.0, 0.0]
        record(out, u4)
        end_trajectory(out, nothing)
        flush(out)

        h5open(file, "r") do fid
            @test haskey(fid, "trajectories/1")
            d = fid["trajectories/1"]
            @test size(d) == (7, 5)
            @test d[:, 1] == u0
            @test d[:, 2] == u1
            @test d[:, 3] == u2
            @test d[:, 4] == u3
            @test d[:, 5] == u4
        end
    end

    temporaryfile() do file
        col = 5
        out = HDFOutput{Float64}(file, LK, (ROW, col), PROG)

        u0 = [0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0]
        start_trajectory(out, u0, 1)
        u1 = [0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0]
        record(out, u1)
        u2 = [0.0, 2.0, 0.0, 0.0, 1.0, 0.0, 0.0]
        record(out, u2)
        u3 = [0.0, 3.0, 0.0, 0.0, 1.0, 0.0, 0.0]
        record(out, u3)
        u4 = [0.0, 4.0, 0.0, 0.0, 1.0, 0.0, 0.0]
        record(out, u4)
        u5 = [0.0, 5.0, 0.0, 0.0, 1.0, 0.0, 0.0]
        record(out, u5)
        end_trajectory(out, nothing)
        flush(out)

        h5open(file, "r") do fid
            @test haskey(fid, "trajectories/1")
            d = fid["trajectories/1"]
            @test size(d) == (7, 6)
            @test d[:, 1] == u0
            @test d[:, 2] == u1
            @test d[:, 3] == u2
            @test d[:, 4] == u3
            @test d[:, 5] == u4
            @test d[:, 6] == u5
        end
    end

    temporaryfile() do file
        col = 5
        out = HDFOutput{Float64}(file, LK, (ROW, col), PROG)

        u10 = [0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0]
        start_trajectory(out, u10, 1)
        u11 = [0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0]
        record(out, u11)
        end_trajectory(out, nothing)

        u20 = [0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0]
        start_trajectory(out, u20, 2)
        u21 = [0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0]
        record(out, u21)
        end_trajectory(out, nothing)
        flush(out)

        h5open(file, "r") do fid
            @test haskey(fid, "trajectories/1")
            d1 = fid["trajectories/1"]
            @test size(d1) == (7, 2)
            @test d1[:, 1] == u10
            @test d1[:, 2] == u11

            @test haskey(fid, "trajectories/2")
            d2 = fid["trajectories/2"]
            @test size(d2) == (7, 2)
            @test d2[:, 1] == u20
            @test d2[:, 2] == u21
        end
    end

    temporaryfile() do file
        col = 5
        out = HDFOutput{Float64}(file, LK, (ROW, col), PROG)

        u10 = [0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0]
        start_trajectory(out, u10, 1)
        u11 = [0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0]
        record(out, u11)
        u12 = [0.0, 2.0, 0.0, 0.0, 1.0, 0.0, 0.0]
        record(out, u12)
        end_trajectory(out, nothing)

        u20 = [0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0]
        start_trajectory(out, u20, 2)
        u21 = [0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0]
        record(out, u21)
        end_trajectory(out, nothing)
        flush(out)

        h5open(file, "r") do fid
            @test haskey(fid, "trajectories/1")
            d1 = fid["trajectories/1"]
            @test size(d1) == (7, 3)
            @test d1[:, 1] == u10
            @test d1[:, 2] == u11
            @test d1[:, 3] == u12

            @test haskey(fid, "trajectories/2")
            d2 = fid["trajectories/2"]
            @test size(d2) == (7, 2)
            @test d2[:, 1] == u20
            @test d2[:, 2] == u21
        end
    end

    temporaryfile() do file
        col = 5
        out = HDFOutput{Float64}(file, LK, (ROW, col), PROG)

        u10 = [0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0]
        start_trajectory(out, u10, 1)
        u11 = [0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0]
        record(out, u11)
        u12 = [0.0, 2.0, 0.0, 0.0, 1.0, 0.0, 0.0]
        record(out, u12)
        end_trajectory(out, nothing)

        u20 = [0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0]
        start_trajectory(out, u20, 2)
        u21 = [0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0]
        record(out, u21)
        u22 = [0.0, 0.0, 2.0, 0.0, 0.0, 1.0, 0.0]
        record(out, u22)
        end_trajectory(out, nothing)
        flush(out)

        h5open(file, "r") do fid
            @test haskey(fid, "trajectories/1")
            d1 = fid["trajectories/1"]
            @test size(d1) == (7, 3)
            @test d1[:, 1] == u10
            @test d1[:, 2] == u11
            @test d1[:, 3] == u12

            @test haskey(fid, "trajectories/2")
            d2 = fid["trajectories/2"]
            @test size(d2) == (7, 3)
            @test d2[:, 1] == u20
            @test d2[:, 2] == u21
            @test d2[:, 3] == u22
        end
    end

    temporaryfile() do file
        col = 2
        out = HDFOutput{Float64}(file, LK, (ROW, col), PROG)

        start_trajectory(out, [0.0 0.0 0.0 0.0 1.0 0.0 0.0], 1)
        record(out, [1.0 1.0 0.0 0.0 1.0 0.0 0.0])
        record(out, [2.0 2.0 0.0 0.0 1.0 0.0 0.0])
        end_trajectory(out, true)
        start_trajectory(out, [0.0 0.0 0.0 0.0 1.0 1.0 0.0], 2)
        record(out, [1.0 1.0 1.0 0.0 1.0 1.0 0.0])
        end_trajectory(out, false)
        flush(out)

        @test number_of_trajectories(file) == 2

        @test number_of_recorded_points(file, 1) == 3
        @test number_of_recorded_points(file, 2) == 2

        @test collect_success_attribute(file) == BitVector((true, false))
        @test collect_success_attribute(file, 1) == BitVector((true,))
        @test collect_success_attribute(file, 2) == BitVector((false,))
        @test collect_success_attribute(file, 1:1) == BitVector((true,))
        @test collect_success_attribute(file, 1:2) == BitVector((true, false))
        @test collect_success_attribute(file, [2, 1]) == BitVector((false, true))

        @test collect_initial_points(file) == [0.0 0.0
                                               0.0 0.0
                                               0.0 0.0
                                               0.0 0.0
                                               1.0 1.0
                                               0.0 1.0
                                               0.0 0.0]
        @test collect_initial_points(file, 1) == [0.0; 0.0; 0.0; 0.0; 1.0; 0.0; 0.0;;]
        @test collect_initial_points(file, 2) == [0.0; 0.0; 0.0; 0.0; 1.0; 1.0; 0.0;;]
        @test collect_initial_points(file, 1:1) == [0.0; 0.0; 0.0; 0.0; 1.0; 0.0; 0.0;;]
        @test collect_initial_points(file, 2:2) == [0.0; 0.0; 0.0; 0.0; 1.0; 1.0; 0.0;;]
        @test collect_initial_points(file, [2, 1]) == [0.0 0.0
                                                       0.0 0.0
                                                       0.0 0.0
                                                       0.0 0.0
                                                       1.0 1.0
                                                       1.0 0.0
                                                       0.0 0.0]
        @test collect_initial_points(file, BitVector((true, false))) == [0.0; 0.0; 0.0; 0.0; 1.0; 0.0; 0.0;;]
        @test collect_initial_points(file, BitVector((false, true))) == [0.0; 0.0; 0.0; 0.0; 1.0; 1.0; 0.0;;]

        @test collect_end_points(file) == [2.0 1.0
                                           2.0 1.0
                                           0.0 1.0
                                           0.0 0.0
                                           1.0 1.0
                                           0.0 1.0
                                           0.0 0.0]
        @test collect_end_points(file, 1) == [2.0; 2.0; 0.0; 0.0; 1.0; 0.0; 0.0;;]
        @test collect_end_points(file, 2) == [1.0; 1.0; 1.0; 0.0; 1.0; 1.0; 0.0;;]
        @test collect_end_points(file, 1:1) == [2.0; 2.0; 0.0; 0.0; 1.0; 0.0; 0.0;;]
        @test collect_end_points(file, 2:2) == [1.0; 1.0; 1.0; 0.0; 1.0; 1.0; 0.0;;]
        @test collect_end_points(file, [2, 1]) == [1.0 2.0
                                                   1.0 2.0
                                                   1.0 0.0
                                                   0.0 0.0
                                                   1.0 1.0
                                                   1.0 0.0
                                                   0.0 0.0]
        @test collect_end_points(file, BitVector((true, false))) == [2.0; 2.0; 0.0; 0.0; 1.0; 0.0; 0.0;;]
        @test collect_end_points(file, BitVector((false, true))) == [1.0; 1.0; 1.0; 0.0; 1.0; 1.0; 0.0;;]

        @test collect_trajectories(file) == [
                [0.0 1.0 2.0
                 0.0 1.0 2.0
                 0.0 0.0 0.0
                 0.0 0.0 0.0
                 1.0 1.0 1.0
                 0.0 0.0 0.0
                 0.0 0.0 0.0],
                [0.0 1.0
                 0.0 1.0
                 0.0 1.0
                 0.0 0.0
                 1.0 1.0
                 1.0 1.0
                 0.0 0.0]
              ]
        @test collect_trajectories(file, 1) == [
                [0.0 1.0 2.0
                 0.0 1.0 2.0
                 0.0 0.0 0.0
                 0.0 0.0 0.0
                 1.0 1.0 1.0
                 0.0 0.0 0.0
                 0.0 0.0 0.0]
              ]
        @test collect_trajectories(file, 2) == [
                [0.0 1.0
                 0.0 1.0
                 0.0 1.0
                 0.0 0.0
                 1.0 1.0
                 1.0 1.0
                 0.0 0.0]
              ]
        @test collect_trajectories(file, 1:1) == [
                [0.0 1.0 2.0
                 0.0 1.0 2.0
                 0.0 0.0 0.0
                 0.0 0.0 0.0
                 1.0 1.0 1.0
                 0.0 0.0 0.0
                 0.0 0.0 0.0]
              ]
        @test collect_trajectories(file, 2:2) == [
                [0.0 1.0
                 0.0 1.0
                 0.0 1.0
                 0.0 0.0
                 1.0 1.0
                 1.0 1.0
                 0.0 0.0]
              ]
        @test collect_trajectories(file, 1:2) == [
                [0.0 1.0 2.0
                 0.0 1.0 2.0
                 0.0 0.0 0.0
                 0.0 0.0 0.0
                 1.0 1.0 1.0
                 0.0 0.0 0.0
                 0.0 0.0 0.0],
                [0.0 1.0
                 0.0 1.0
                 0.0 1.0
                 0.0 0.0
                 1.0 1.0
                 1.0 1.0
                 0.0 0.0]
              ]
        @test collect_trajectories(file, [2, 1]) == [
                [0.0 1.0
                 0.0 1.0
                 0.0 1.0
                 0.0 0.0
                 1.0 1.0
                 1.0 1.0
                 0.0 0.0],
                [0.0 1.0 2.0
                 0.0 1.0 2.0
                 0.0 0.0 0.0
                 0.0 0.0 0.0
                 1.0 1.0 1.0
                 0.0 0.0 0.0
                 0.0 0.0 0.0]
              ]
        @test collect_trajectories(file, BitVector((true, false))) == [
                [0.0 1.0 2.0
                 0.0 1.0 2.0
                 0.0 0.0 0.0
                 0.0 0.0 0.0
                 1.0 1.0 1.0
                 0.0 0.0 0.0
                 0.0 0.0 0.0]
              ]
        @test collect_trajectories(file, BitVector((false, true))) == [
                [0.0 1.0
                 0.0 1.0
                 0.0 1.0
                 0.0 0.0
                 1.0 1.0
                 1.0 1.0
                 0.0 0.0]
              ]

        h5open(file, "r") do fid
            @test number_of_trajectories(fid) == 2

            @test number_of_recorded_points(fid, 1) == 3
            @test number_of_recorded_points(fid, 2) == 2

            @test collect_initial_points(fid) == [0.0 0.0
                                                  0.0 0.0
                                                  0.0 0.0
                                                  0.0 0.0
                                                  1.0 1.0
                                                  0.0 1.0
                                                  0.0 0.0]
            @test collect_initial_points(fid, 1) == [0.0; 0.0; 0.0; 0.0; 1.0; 0.0; 0.0;;]
            @test collect_initial_points(fid, 2) == [0.0; 0.0; 0.0; 0.0; 1.0; 1.0; 0.0;;]
            @test collect_initial_points(fid, 1:1) == [0.0; 0.0; 0.0; 0.0; 1.0; 0.0; 0.0;;]
            @test collect_initial_points(fid, 2:2) == [0.0; 0.0; 0.0; 0.0; 1.0; 1.0; 0.0;;]
            @test collect_initial_points(fid, [2, 1]) == [0.0 0.0
                                                          0.0 0.0
                                                          0.0 0.0
                                                          0.0 0.0
                                                          1.0 1.0
                                                          1.0 0.0
                                                          0.0 0.0]
            @test collect_initial_points(fid, BitVector((true, false))) == [0.0; 0.0; 0.0; 0.0; 1.0; 0.0; 0.0;;]
            @test collect_initial_points(fid, BitVector((false, true))) == [0.0; 0.0; 0.0; 0.0; 1.0; 1.0; 0.0;;]

            @test collect_end_points(fid) == [2.0 1.0
                                              2.0 1.0
                                              0.0 1.0
                                              0.0 0.0
                                              1.0 1.0
                                              0.0 1.0
                                              0.0 0.0]
            @test collect_end_points(fid, 1) == [2.0; 2.0; 0.0; 0.0; 1.0; 0.0; 0.0;;]
            @test collect_end_points(fid, 2) == [1.0; 1.0; 1.0; 0.0; 1.0; 1.0; 0.0;;]
            @test collect_end_points(fid, 1:1) == [2.0; 2.0; 0.0; 0.0; 1.0; 0.0; 0.0;;]
            @test collect_end_points(fid, 2:2) == [1.0; 1.0; 1.0; 0.0; 1.0; 1.0; 0.0;;]
            @test collect_end_points(fid, [2, 1]) == [1.0 2.0
                                                      1.0 2.0
                                                      1.0 0.0
                                                      0.0 0.0
                                                      1.0 1.0
                                                      1.0 0.0
                                                      0.0 0.0]
            @test collect_end_points(fid, BitVector((true, false))) == [2.0; 2.0; 0.0; 0.0; 1.0; 0.0; 0.0;;]
            @test collect_end_points(fid, BitVector((false, true))) == [1.0; 1.0; 1.0; 0.0; 1.0; 1.0; 0.0;;]
        end
    end
end
