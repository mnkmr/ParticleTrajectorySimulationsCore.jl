# TODO: Need more sophisticated tests
using Test
using ParticleTrajectorySimulationsCore
using ParticleTrajectorySimulationsCore.Apparatuses:
    pass!
using ParticleTrajectorySimulationsCore.Outputs:
    DummyOutput

s = 2*sqrt(eps())

@testset "Hexapole" begin
    p1 = BasicSymmetricTop1stOrder("methyl iodide", 141.94, 0.25, 1.6406,  1, 1, -1)
    hexapole1 = Hexapole((0.0, 0.0, 1.0), (0, 0, 0), 0.5, 0.005, 0.0)
    out = DummyOutput()
    @test pass!([0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1000.0], hexapole1, p1, out) == true

    θ = atan(0.005/1.5)
    @test pass!([0.0, 0.0, 0.0, 0.0, 0.0, 1000.0*sin(θ)-s, 1000.0*cos(θ)], hexapole1, p1, out) == true
    @test pass!([0.0, 0.0, 0.0, 0.0, 0.0, 1000.0*sin(θ)+s, 1000.0*cos(θ)], hexapole1, p1, out) == false
    @test pass!([0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1000.0*cos(π)], hexapole1, p1, out) == false
    @test pass!([0.0, 0.0, 0.0, 0.0, 0.0, 1000.0*sin(π/4), 1000.0*cos(θ)], hexapole1, p1, out) == false

    hexapole2 = Hexapole((0.0, 0.0, 1.0), (0, 0, 0), 0.5, 0.005, 1000.0)
    @test pass!([0.0, 0.0, 0.0, 0.0, 0.0, 1000.0*sin(θ)+s, 1000.0*cos(θ)], hexapole2, p1, out) == true

    p2 = BasicSymmetricTop1stOrder("methyl iodide", 141.94, 0.25, 1.6406,  1, 1, 1)
    @test pass!([0.0, 0.0, 0.0, 0.0, 0.0, 1000.0*sin(θ)-s, 1000.0*cos(θ)], hexapole2, p2, out) == false

    @test Hexapole((0.0, 0.0, 1.0), 0.5, 0.005, 1000.0) ==
            Hexapole((0.0, 0.0, 1.0), (0, 0, 0), 0.5, 0.005, 1000.0)
end

@testset "Deflector" begin
    p1 = BasicSymmetricTop1stOrder("methyl iodide", 141.94, 0.25, 1.6406,  1, 1, -1)
    deflector1 = Deflector((0.0, 0.0, 1.0), (0, 0, 0), 0.5, 1.0e-3, (1.2, 0.41, 0.0), 0.0, 0.0)
    out = DummyOutput()
    @test pass!([0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1000.0], deflector1, p1, out) == true

    θ = atan(0.0007417/1.5)
    @test pass!([0.0, 0.0, 0.0, 0.0, 0.0, 1000.0*sin(θ), 1000.0*cos(θ)], deflector1, p1, out) == false
    @test pass!([0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1000.0*cos(π)], deflector1, p1, out) == false
    @test pass!([0.0, 0.0, 0.0, 0.0, 0.0, 1000.0*sin(π/4), 1000.0*cos(θ)], deflector1, p1, out) == false

    deflector2 = Deflector((0.0, 0.0, 1.0), (0, 0, 0), 0.5, 1.0e-3, (1.2, 0.41, 0.0), 1000.0, 0.0)
    @test pass!([0.0, 0.0, 0.0, 0.0, 0.0, 1000.0*sin(θ), 1000.0*cos(θ)], deflector2, p1, out) == true

    p2 = BasicSymmetricTop1stOrder("methyl iodide", 141.94, 0.25, 1.6406,  1, 1, 1)
    @test pass!([0.0, 0.0, 0.0, 0.0, 0.0, 1000.0*sin(θ), 1000.0*cos(θ)], deflector2, p2, out) == false

    @test Deflector((0.0, 0.0, 1.0), 0.5, 1.0e-3, (1.2, 0.41, 0.0), 1000.0, 0.0) ==
            Deflector((0.0, 0.0, 1.0), (0, 0, 0), 0.5, 1.0e-3, (1.2, 0.41, 0.0), 1000.0, 0.0)
end
