using ParticleTrajectorySimulationsCore
using Test

≈(x::Float64, y::Float64) = isapprox(x, y, atol=eps(), rtol=sqrt(eps()))

@testset "ParticleTrajectorySimulationsCore.jl" begin
    include("apparatus.jl")
    include("electrodes.jl")
    include("output.jl")
    include("progress.jl")
    include("threading.jl")
end
