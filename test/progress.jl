using Test
using ParticleTrajectorySimulationsCore
using ParticleTrajectorySimulationsCore.Progress


@testset "CountProgress{Enabled,Int}" begin
    @test CountProgress(1, stdout, 1.0, true) isa CountProgress{Progress.Enabled,Int}

    io = IOBuffer()
    cp = CountProgress(2; io=io, dt=100)
    @test cp.simulated == 0
    @test cp.recorded == 0

    # do not print in a short time from the start
    simulation_next!(cp)
    @test cp.simulated == 1
    @test cp.recorded == 0
    @test isempty(String(take!(io)))

    # time passed...
    now = time()
    cp.t0 = now - 100*1
    cp.tprev = now - 100
    simulation_next!(cp)
    @test cp.simulated == 2
    @test cp.recorded == 0
    str = String(take!(io))
    @test !isempty(str)
    @test !startswith(str, "\u1b[1G\u1b[K")

    # do not print in a short time from the previous printing
    now = time()
    cp.tprev = now
    record_next!(cp)
    @test cp.simulated == 2
    @test cp.recorded == 1
    @test isempty(String(take!(io)))

    # time passed...
    now = time()
    cp.t0 = now - 100*2
    cp.tprev = now - 100
    record_next!(cp)
    @test cp.simulated == 2
    @test cp.recorded == 2
    @test startswith(String(take!(io)), "\u1b[1G\u1b[K")

    # print the final result
    finish!(cp)
    str = String(take!(io))
    @test startswith(str, "\u1b[1G\u1b[K")
    @test endswith(str, "✓\n")


    # do not print if everything finishes in a short time
    take!(io)
    cp = CountProgress(1; io=io, dt=100)
    simulation_next!(cp)
    record_next!(cp)
    finish!(cp)
    str = String(take!(io))
    @test isempty(str)


    # print the final result if an adequate time passed from the start
    take!(io)
    cp = CountProgress(1; io=io, dt=100)
    simulation_next!(cp)
    record_next!(cp)
    @test isempty(String(take!(io)))
    # an adequate time passed from the start but not from the previous printing
    now = time()
    cp.t0 = now - 100
    cp.tprev = now
    finish!(cp)
    str = String(take!(io))
    @test !isempty(str)
    @test !startswith(str, "\u1b[1G\u1b[K")
    @test endswith(str, "✓\n")


    # print the final result if printed once in the past
    take!(io)
    cp = CountProgress(1; io=io, dt=100)
    # time passed...
    now = time()
    cp.t0 = now - 100
    cp.tprev = now - 100
    simulation_next!(cp)
    record_next!(cp)
    @test !isempty(String(take!(io)))
    finish!(cp)
    str = String(take!(io))
    @test !isempty(str)
    @test startswith(str, "\u1b[1G\u1b[K")
    @test endswith(str, "✓\n")
end


@testset "CountProgress{Enabled,Nothing}" begin
    @test CountProgress(nothing, stdout, 1.0, true) isa CountProgress{Progress.Enabled,Nothing}

    io = IOBuffer()
    cp = CountProgress(nothing; io=io, dt=100)
    @test cp.simulated == 0
    @test cp.recorded == 0

    # do not print in a short time from the start
    simulation_next!(cp)
    @test cp.simulated == 1
    @test cp.recorded == 0
    @test isempty(String(take!(io)))

    # time passed...
    now = time()
    cp.t0 = now - 100*1
    cp.tprev = now - 100
    simulation_next!(cp)
    @test cp.simulated == 2
    @test cp.recorded == 0
    str = String(take!(io))
    @test !isempty(str)
    @test !startswith(str, "\u1b[1G\u1b[K")

    # do not print in a short time from the previous printing
    now = time()
    cp.tprev = now
    record_next!(cp)
    @test cp.simulated == 2
    @test cp.recorded == 1
    @test isempty(String(take!(io)))

    # time passed...
    now = time()
    cp.t0 = now - 100*2
    cp.tprev = now - 100
    record_next!(cp)
    @test cp.simulated == 2
    @test cp.recorded == 2
    @test startswith(String(take!(io)), "\u1b[1G\u1b[K")

    # print the final result
    finish!(cp)
    str = String(take!(io))
    @test startswith(str, "\u1b[1G\u1b[K")
    @test endswith(str, "✓\n")


    # do not print if everything finishes in a short time
    take!(io)
    cp = CountProgress(nothing; io=io, dt=100)
    simulation_next!(cp)
    record_next!(cp)
    finish!(cp)
    str = String(take!(io))
    @test isempty(str)


    # print the final result if an adequate time passed from the start
    take!(io)
    cp = CountProgress(nothing; io=io, dt=100)
    simulation_next!(cp)
    record_next!(cp)
    @test isempty(String(take!(io)))
    # an adequate time passed from the start but not from the previous printing
    now = time()
    cp.t0 = now - 100
    cp.tprev = now
    finish!(cp)
    str = String(take!(io))
    @test !isempty(str)
    @test !startswith(str, "\u1b[1G\u1b[K")
    @test endswith(str, "✓\n")


    # print the final result if printed once in the past
    take!(io)
    cp = CountProgress(nothing; io=io, dt=100)
    # time passed...
    now = time()
    cp.t0 = now - 100
    cp.tprev = now - 100
    simulation_next!(cp)
    record_next!(cp)
    @test !isempty(String(take!(io)))
    finish!(cp)
    str = String(take!(io))
    @test !isempty(str)
    @test startswith(str, "\u1b[1G\u1b[K")
    @test endswith(str, "✓\n")
end


@testset "CountProgress{Disabled,Int}" begin
    @test CountProgress(1, stdout, 1.0, false) isa CountProgress{Progress.Disabled,Int}

    io = IOBuffer()
    cp = CountProgress(2; io=io, dt=0.1, show=false)

    # do not print in anyway
    simulation_next!(cp)
    @test isempty(String(take!(io)))

    # time passed...
    now = time()
    cp.t0 = now - 100*1
    cp.tprev = now - 100
    simulation_next!(cp)
    @test isempty(String(take!(io)))

    now = time()
    cp.tprev = now
    record_next!(cp)
    @test isempty(String(take!(io)))

    # time passed...
    now = time()
    cp.t0 = now - 100*2
    cp.tprev = now - 100
    record_next!(cp)
    @test isempty(String(take!(io)))

    # do not print the final result
    finish!(cp)
    @test isempty(String(take!(io)))
end


@testset "CountProgress{Disabled,Nothing}" begin
    @test CountProgress(nothing, stdout, 1.0, false) isa CountProgress{Progress.Disabled,Nothing}

    io = IOBuffer()
    cp = CountProgress(nothing; io=io, dt=0.1, show=false)

    # do not print in anyway
    simulation_next!(cp)
    @test isempty(String(take!(io)))

    # time passed...
    now = time()
    cp.t0 = now - 100*1
    cp.tprev = now - 100
    simulation_next!(cp)
    @test isempty(String(take!(io)))

    now = time()
    cp.tprev = now
    record_next!(cp)
    @test isempty(String(take!(io)))

    # time passed...
    now = time()
    cp.t0 = now - 100*2
    cp.tprev = now - 100
    record_next!(cp)
    @test isempty(String(take!(io)))

    # do not print the final result
    finish!(cp)
    @test isempty(String(take!(io)))
end
