using Test
using Distributions: Uniform
using StaticArrays
using ParticleTrajectorySimulationsCore
using ParticleTrajectorySimulationsCore.Apparatuses:
    crossed_at,
    Plane,
    Circle,
    Rectangle,
    pass!,
    emit!
using ParticleTrajectorySimulationsCore.Outputs:
    DummyOutput
using LinearAlgebra:
    norm

s = 2*sqrt(eps())
p = BasicSymmetricTop("", 1.0, 1.0, 1.0, 0.0, 0.0, 0.0) # dummy
out = DummyOutput()

@testset "Plane" begin
    let
        coordinates = SA[0.0, 0.0, 0.0]
        orientation = SA[0.0, 0.0, 0.0]
        @test Plane(coordinates, orientation) isa Plane
    end

    let
        coordinates = [0.0, 0.0, 0.0]
        orientation = [0.0, 0.0, 0.0]
        @test Plane(coordinates, orientation) isa Plane
    end

    let
        coordinates = [0, 0, 0]
        orientation = [0.0, 0.0, 0.0f0]
        @test Plane(coordinates, orientation) isa Plane
    end

    let pl = Plane([0.0, 0.0, 0.0], [0.0, 0.0, 0.0])
        t0 = 0
        c0 = [0, 0,  1]
        v0 = [0, 0, -1]
        crossed, uc = crossed_at(pl, [t0, c0..., v0...])
        @test crossed === true
        @test uc[1] ≈  1.0
        @test uc[2] ≈  0.0
        @test uc[3] ≈  0.0
        @test uc[4] ≈  0.0
        @test uc[5] ≈  0.0
        @test uc[6] ≈  0.0
        @test uc[7] ≈ -1.0

        t0 = 0
        c0 = [0, 0, -1]
        v0 = [0, 0,  1]
        crossed, uc = crossed_at(pl, [t0, c0..., v0...])
        @test crossed === true
        @test uc[1] ≈ 1.0
        @test uc[2] ≈ 0.0
        @test uc[3] ≈ 0.0
        @test uc[4] ≈ 0.0
        @test uc[5] ≈ 0.0
        @test uc[6] ≈ 0.0
        @test uc[7] ≈ 1.0

        t0 = 0
        c0 = [0, 0, 1]
        v0 = [0, 0, 1]
        crossed, uc = crossed_at(pl, [t0, c0..., v0...])
        @test crossed === false
        @test uc === nothing

        t0 = 0
        c0 = [0, 0, -1]
        v0 = [0, 0, -1]
        crossed, uc = crossed_at(pl, [t0, c0..., v0...])
        @test crossed === false
        @test uc === nothing

        t0 = 0
        c0 = [0,  1, 0]
        v0 = [0, -1, 0]
        crossed, uc = crossed_at(pl, [t0, c0..., v0...])
        @test crossed === false
        @test uc === nothing

        t0 = 0
        c0 = [0, -1, 0]
        v0 = [0,  1, 0]
        crossed, uc = crossed_at(pl, [t0, c0..., v0...])
        @test crossed === false
        @test uc === nothing

        t0 = 0
        c0 = [0,  1, 0]
        v0 = [0,  1, 0]
        crossed, uc = crossed_at(pl, [t0, c0..., v0...])
        @test crossed === false
        @test uc === nothing

        t0 = 0
        c0 = [0, -1, 0]
        v0 = [0, -1, 0]
        crossed, uc = crossed_at(pl, [t0, c0..., v0...])
        @test crossed === false
        @test uc === nothing

        t0 = 0
        c0 = [ 1, 0, 0]
        v0 = [-1, 0, 0]
        crossed, uc = crossed_at(pl, [t0, c0..., v0...])
        @test crossed === false
        @test uc === nothing

        t0 = 0
        c0 = [-1, 0, 0]
        v0 = [ 1, 0, 0]
        crossed, uc = crossed_at(pl, [t0, c0..., v0...])
        @test crossed === false
        @test uc === nothing

        t0 = 0
        c0 = [ 1, 0, 0]
        v0 = [ 1, 0, 0]
        crossed, uc = crossed_at(pl, [t0, c0..., v0...])
        @test crossed === false
        @test uc === nothing

        t0 = 0
        c0 = [-1, 0, 0]
        v0 = [-1, 0, 0]
        crossed, uc = crossed_at(pl, [t0, c0..., v0...])
        @test crossed === false
        @test uc === nothing

        t0 = 0
        c0 = [4, 2, 1]
        v0 = [-2, -1, -1]
        crossed, uc = crossed_at(pl, [t0, c0..., v0...])
        @test crossed === true
        @test uc[1] ≈  1.0
        @test uc[2] ≈  2.0
        @test uc[3] ≈  1.0
        @test uc[4] ≈  0.0
        @test uc[5] ≈ -2.0
        @test uc[6] ≈ -1.0
        @test uc[7] ≈ -1.0

        t0 = 0
        c0 = [2, 1, 0]
        v0 = [0, 0,  1]
        crossed, uc = crossed_at(pl, [t0, c0..., v0...])
        @test crossed === true
        @test uc[1] ≈ 0.0
        @test uc[2] ≈ 2.0
        @test uc[3] ≈ 1.0
        @test uc[4] ≈ 0.0
        @test uc[5] ≈ 0.0
        @test uc[6] ≈ 0.0
        @test uc[7] ≈ 1.0

        t0 = 0
        c0 = [2, 1, 0]
        v0 = [0, 0, -1]
        crossed, uc = crossed_at(pl, [t0, c0..., v0...])
        @test crossed === true
        @test uc[1] ≈  0.0
        @test uc[2] ≈  2.0
        @test uc[3] ≈  1.0
        @test uc[4] ≈  0.0
        @test uc[5] ≈  0.0
        @test uc[6] ≈  0.0
        @test uc[7] ≈ -1.0

        t0 = 0
        c0 = [2, 1, 0]
        v0 = [0, 0,  0]
        crossed, uc = crossed_at(pl, [t0, c0..., v0...])
        @test crossed === false
        @test uc === nothing

        t0 = 0
        c0 = [0,  4, 0]
        v0 = [0, -1, 0]
        crossed, uc = crossed_at(pl, [t0, c0..., v0...])
        @test crossed === false
        @test uc === nothing
    end

    let pl = Plane([0.0, 1.0, 0.0], [π/2, π/2, 0])
        t0 = 0
        c0 = [0, 2, 1]
        v0 = [0, -1, 0]
        crossed, uc = crossed_at(pl, [t0, c0..., v0...])
        @test crossed === true
        @test uc[1] ≈  1.0
        @test uc[2] ≈  0.0
        @test uc[3] ≈  1.0
        @test uc[4] ≈  1.0
        @test uc[5] ≈  0.0
        @test uc[6] ≈ -1.0
        @test uc[7] ≈  0.0

        t0 = 0
        c0 = [0, 2, 1]
        v0 = [0, 1, 0]
        crossed, uc = crossed_at(pl, [t0, c0..., v0...])
        @test crossed === false
        @test uc === nothing

        t0 = 0
        c0 = [0, 2, 1]
        v0 = [1, 0, 0]
        crossed, uc = crossed_at(pl, [t0, c0..., v0...])
        @test crossed === false
        @test uc === nothing

        t0 = 0
        c0 = [0, 2, 1]
        v0 = [-1, 0, 0]
        crossed, uc = crossed_at(pl, [t0, c0..., v0...])
        @test crossed === false
        @test uc === nothing

        t0 = 0
        c0 = [0, 2, 1]
        v0 = [0, 0, 1]
        crossed, uc = crossed_at(pl, [t0, c0..., v0...])
        @test crossed === false
        @test uc === nothing

        t0 = 0
        c0 = [0, 2, 1]
        v0 = [0, 0, -1]
        crossed, uc = crossed_at(pl, [t0, c0..., v0...])
        @test crossed === false
        @test uc === nothing
    end
end


@testset "Circle" begin
    let
        coordinates = SA[0.0, 0.0, 0.0]
        orientation = SA[0.0, 0.0, 0.0]
        radius = 1.0
        @test Circle(coordinates, orientation, radius) isa Circle
    end

    let
        coordinates = [0.0, 0.0, 0.0]
        orientation = [0.0, 0.0, 0.0]
        radius = 1.0
        @test Circle(coordinates, orientation, radius) isa Circle
    end

    let
        coordinates = [0.0, 0, 0]
        orientation = [0.0, 0.0, 0]
        radius = 1.0f0
        @test Circle(coordinates, orientation, radius) isa Circle
    end

    let circ = Circle([0.0, 0.0, 0.0], [0.0, 0.0, 0.0], 1.0)
        t0 = 0
        c0 = [0.0, 0.0, -1.0]
        v0 = [0.0, 0.0, 1.0]
        crossed, uc = crossed_at(circ, [t0, c0..., v0...])
        @test crossed === true
        @test uc[1] ≈ 1.0
        @test uc[2] ≈ 0.0
        @test uc[3] ≈ 0.0
        @test uc[4] ≈ 0.0
        @test uc[5] ≈ 0.0
        @test uc[6] ≈ 0.0
        @test uc[7] ≈ 1.0

        t0 = 0
        c0 = [0.0, 0.0, 1.0]
        v0 = [0.0, 0.0, 1.0]
        crossed, uc = crossed_at(circ, [t0, c0..., v0...])
        @test crossed === false
        @test uc === nothing

        t0 = 0
        c0 = [1.0, 0.0, 1.0]
        v0 = [0.0, 0.0, -1.0]
        crossed, uc = crossed_at(circ, [t0, c0..., v0...])
        @test crossed === true
        @test uc[1] ≈  1.0
        @test uc[2] ≈  1.0
        @test uc[3] ≈  0.0
        @test uc[4] ≈  0.0
        @test uc[5] ≈  0.0
        @test uc[6] ≈  0.0
        @test uc[7] ≈ -1.0

        t0 = 0
        c0 = [1.0+s, 0.0, 1.0]
        v0 = [0.0, 0.0, -1.0]
        crossed, uc = crossed_at(circ, [t0, c0..., v0...])
        @test crossed === false
        @test uc[1] ≈  1.0
        @test uc[2] ≈  1.0+s
        @test uc[3] ≈  0.0
        @test uc[4] ≈  0.0
        @test uc[5] ≈  0.0
        @test uc[6] ≈  0.0
        @test uc[7] ≈ -1.0

        t0 = 0
        c0 = [0.0, 1.0, 1.0]
        v0 = [0.0, 0.0, -1.0]
        crossed, uc = crossed_at(circ, [t0, c0..., v0...])
        @test crossed === true
        @test uc[1] ≈ 1.0
        @test uc[2] ≈ 0.0
        @test uc[3] ≈ 1.0
        @test uc[4] ≈ 0.0
        @test uc[5] ≈ 0.0
        @test uc[6] ≈ 0.0
        @test uc[7] ≈ -1.0

        t0 = 0
        c0 = [0.0, 1.0+s, 1.0]
        v0 = [0.0, 0.0, -1.0]
        crossed, uc = crossed_at(circ, [t0, c0..., v0...])
        @test crossed === false
        @test uc[1] ≈ 1.0
        @test uc[2] ≈ 0.0
        @test uc[3] ≈ 1.0+s
        @test uc[4] ≈ 0.0
        @test uc[5] ≈ 0.0
        @test uc[6] ≈ 0.0
        @test uc[7] ≈ -1.0

        t0 = 0
        c0 = [2.0, 1.5, 1.0]
        v0 = [-1.5, -1.0, -1.0]
        crossed, uc = crossed_at(circ, [t0, c0..., v0...])
        @test crossed === true
        @test uc[1] ≈  1.0
        @test uc[2] ≈  0.5
        @test uc[3] ≈  0.5
        @test uc[4] ≈  0.0
        @test uc[5] ≈ -1.5
        @test uc[6] ≈ -1.0
        @test uc[7] ≈ -1.0
    end

    let circ = Circle([0.0, 1.0, 0.0], [π/2, π/2, 0], 1.0)
        t0 = 0
        c0 = [0, 2, 1]
        v0 = [0, -1, 0]
        crossed, uc = crossed_at(circ, [t0, c0..., v0...])
        @test crossed === true
        @test uc[1] ≈  1.0
        @test uc[2] ≈  0.0
        @test uc[3] ≈  1.0
        @test uc[4] ≈  1.0
        @test uc[5] ≈  0.0
        @test uc[6] ≈ -1.0
        @test uc[7] ≈  0.0

        t0 = 0
        c0 = [0.0, 2.0, 1.0+s]
        v0 = [0, -1, 0]
        crossed, uc = crossed_at(circ, [t0, c0..., v0...])
        @test crossed === false
        @test uc[1] ≈  1.0
        @test uc[2] ≈  0.0
        @test uc[3] ≈  1.0
        @test uc[4] ≈  1.0+s
        @test uc[5] ≈  0.0
        @test uc[6] ≈ -1.0
        @test uc[7] ≈  0.0

        t0 = 0
        c0 = [0, 2, 1]
        v0 = [0, 1, 0]
        crossed, uc = crossed_at(circ, [t0, c0..., v0...])
        @test crossed === false
        @test uc === nothing

        t0 = 0
        c0 = [0, 2, 1]
        v0 = [1, 0, 0]
        crossed, uc = crossed_at(circ, [t0, c0..., v0...])
        @test crossed === false
        @test uc === nothing

        t0 = 0
        c0 = [0, 2, 1]
        v0 = [-1, 0, 0]
        crossed, uc = crossed_at(circ, [t0, c0..., v0...])
        @test crossed === false
        @test uc === nothing

        t0 = 0
        c0 = [0, 2, 1]
        v0 = [0, 0, 1]
        crossed, uc = crossed_at(circ, [t0, c0..., v0...])
        @test crossed === false
        @test uc === nothing

        t0 = 0
        c0 = [0, 2, 1]
        v0 = [0, 0, -1]
        crossed, uc = crossed_at(circ, [t0, c0..., v0...])
        @test crossed === false
        @test uc === nothing
    end
end


@testset "Rectangle" begin
    let
        coordinates = SA[0.0, 0.0, 0.0]
        orientation = SA[0.0, 0.0, 0.0]
        width = 1.0
        height = 1.0
        @test Rectangle(coordinates, orientation, width, height) isa Rectangle
    end

    let
        coordinates = [0.0, 0.0, 0.0]
        orientation = [0.0, 0.0, 0.0]
        width = 1.0
        height = 1.0
        @test Rectangle(coordinates, orientation, width, height) isa Rectangle
    end

    let
        coordinates = [0.0, 0, 0]
        orientation = [0.0, 0.0, 0]
        width = 1.0f0
        height = 1.0
        @test Rectangle(coordinates, orientation, width, height) isa Rectangle
    end

    let rect = Rectangle([0.0, 0.0, 0.0], [0.0, 0.0, 0.0], 2.0, 1.0)
        t0 = 0
        c0 = [0.0, 0.0, -1.0]
        v0 = [0.0, 0.0, 1.0]
        crossed, uc = crossed_at(rect, [t0, c0..., v0...])
        @test crossed === true
        @test uc[1] ≈ 1.0
        @test uc[2] ≈ 0.0
        @test uc[3] ≈ 0.0
        @test uc[4] ≈ 0.0
        @test uc[5] ≈ 0.0
        @test uc[6] ≈ 0.0
        @test uc[7] ≈ 1.0

        t0 = 0
        c0 = [0.0, 0.0, 1.0]
        v0 = [0.0, 0.0, 1.0]
        crossed, uc = crossed_at(rect, [t0, c0..., v0...])
        @test crossed === false
        @test uc === nothing

        t0 = 0
        c0 = [1.0, 0.0, 1.0]
        v0 = [0.0, 0.0, -1.0]
        crossed, uc = crossed_at(rect, [t0, c0..., v0...])
        @test crossed === true
        @test uc[1] ≈  1.0
        @test uc[2] ≈  1.0
        @test uc[3] ≈  0.0
        @test uc[4] ≈  0.0
        @test uc[5] ≈  0.0
        @test uc[6] ≈  0.0
        @test uc[7] ≈ -1.0

        t0 = 0
        c0 = [1.0+s, 0.0, 1.0]
        v0 = [0.0, 0.0, -1.0]
        crossed, uc = crossed_at(rect, [t0, c0..., v0...])
        @test crossed === false
        @test uc[1] ≈  1.0
        @test uc[2] ≈  1.0+s
        @test uc[3] ≈  0.0
        @test uc[4] ≈  0.0
        @test uc[5] ≈  0.0
        @test uc[6] ≈  0.0
        @test uc[7] ≈ -1.0

        t0 = 0
        c0 = [0.0, 0.5, 1.0]
        v0 = [0.0, 0.0, -1.0]
        crossed, uc = crossed_at(rect, [t0, c0..., v0...])
        @test crossed === true
        @test uc[1] ≈  1.0
        @test uc[2] ≈  0.0
        @test uc[3] ≈  0.5
        @test uc[4] ≈  0.0
        @test uc[5] ≈  0.0
        @test uc[6] ≈  0.0
        @test uc[7] ≈ -1.0

        t0 = 0
        c0 = [0.0, 0.5+s, 1.0]
        v0 = [0.0, 0.0, -1.0]
        crossed, uc = crossed_at(rect, [t0, c0..., v0...])
        @test crossed === false
        @test uc[1] ≈  1.0
        @test uc[2] ≈  0.0
        @test uc[3] ≈  0.5+s
        @test uc[4] ≈  0.0
        @test uc[5] ≈  0.0
        @test uc[6] ≈  0.0
        @test uc[7] ≈ -1.0

        t0 = 0
        c0 = [2.0, 1.5, 1.0]
        v0 = [-1.5, -1.0, -1.0]
        crossed, uc = crossed_at(rect, [t0, c0..., v0...])
        @test crossed === true
        @test uc[1] ≈  1.0
        @test uc[2] ≈  0.5
        @test uc[3] ≈  0.5
        @test uc[4] ≈  0.0
        @test uc[5] ≈ -1.5
        @test uc[6] ≈ -1.0
        @test uc[7] ≈ -1.0
    end

    let rect = Rectangle([0.0, 1.0, 0.0], [π/2, π/2, -π/2], 2.0, 1.0)
        t0 = 0
        c0 = [1.0, 2.0, 0.0]
        v0 = [0.0, -1.0, 0.0]
        crossed, uc = crossed_at(rect, [t0, c0..., v0...])
        @test crossed === true
        @test uc[1] ≈ 1.0
        @test uc[2] ≈ 1.0
        @test uc[3] ≈ 1.0
        @test uc[4] ≈ 0.0
        @test uc[5] ≈ 0.0
        @test uc[6] ≈ -1.0
        @test uc[7] ≈ 0.0

        t0 = 0
        c0 = [1.0+s, 2.0, 0.0]
        v0 = [0.0, -1.0, 0.0]
        crossed, uc = crossed_at(rect, [t0, c0..., v0...])
        @test crossed === false
        @test uc[1] ≈ 1.0
        @test uc[2] ≈ 1.0+s
        @test uc[3] ≈ 1.0
        @test uc[4] ≈ 0.0
        @test uc[5] ≈ 0.0
        @test uc[6] ≈ -1.0
        @test uc[7] ≈ 0.0

        t0 = 0
        c0 = [0.0, 2.0, 0.5]
        v0 = [0.0, -1.0, 0.0]
        crossed, uc = crossed_at(rect, [t0, c0..., v0...])
        @test crossed === true
        @test uc[1] ≈ 1.0
        @test uc[2] ≈ 0.0
        @test uc[3] ≈ 1.0
        @test uc[4] ≈ 0.5
        @test uc[5] ≈ 0.0
        @test uc[6] ≈ -1.0
        @test uc[7] ≈ 0.0

        t0 = 0
        c0 = [0.0, 2.0, 0.5+s]
        v0 = [0.0, -1.0, 0.0]
        crossed, uc = crossed_at(rect, [t0, c0..., v0...])
        @test crossed === false
        @test uc[1] ≈ 1.0
        @test uc[2] ≈ 0.0
        @test uc[3] ≈ 1.0
        @test uc[4] ≈ 0.5+s
        @test uc[5] ≈ 0.0
        @test uc[6] ≈ -1.0
        @test uc[7] ≈ 0.0
    end
end


@testset "CircularObstacle" begin
    let
        coordinates = SA[0.0, 0.0, 1.0]
        orientation = SA[0.0, 0.0, 0.0]
        radius = 2.0
        @test CircularObstacle(coordinates, orientation, radius) isa CircularObstacle
    end

    let
        coordinates = [0, 0, 1]
        orientation = [0, 0, 0]
        radius = 2
        @test CircularObstacle(coordinates, orientation, radius) isa CircularObstacle
    end

    let
        coordinates = [0, 0, 1]
        orientation = [0.0, 0.0, 0.0]
        radius = 2.0f0
        @test CircularObstacle(coordinates, orientation, radius) isa CircularObstacle
    end

    let
        coordinates = [0, 0, 0]
        radius = 2.0
        @test CircularObstacle(coordinates, radius) isa CircularObstacle
    end

    let circ_obs = CircularObstacle([0.0, 0.0, 1.0], [0.0, 0.0, 0.0], 1.0)
        u = [0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0]
        passed = pass!(u, circ_obs, p, out)
        @test passed === false
        @test u[1] ≈ 1.0
        @test u[2] ≈ 0.0
        @test u[3] ≈ 1.0
        @test u[4] ≈ 1.0
        @test u[5] ≈ 0.0
        @test u[6] ≈ 1.0
        @test u[7] ≈ 1.0
    end

    let circ_obs = CircularObstacle([0.0, 0.0, 1.0], [0.0, 0.0, 0.0], 1.0)
        u = [0.0, 0.0, -1.0, 0.0, 0.0, 1.0, 1.0]
        passed = pass!(u, circ_obs, p, out)
        @test passed === false
        @test u[1] ≈ 1.0
        @test u[2] ≈ 0.0
        @test u[3] ≈ 0.0
        @test u[4] ≈ 1.0
        @test u[5] ≈ 0.0
        @test u[6] ≈ 1.0
        @test u[7] ≈ 1.0
    end

    let circ_obs = CircularObstacle([0.0, 0.0, 1.0], [0.0, 0.0, 0.0], 1.0)
        u = [0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 1.0]
        passed = pass!(u, circ_obs, p, out)
        @test passed === true
        @test u[1] ≈ 1.0
        @test u[2] ≈ 0.0
        @test u[3] ≈ 2.0
        @test u[4] ≈ 1.0
        @test u[5] ≈ 0.0
        @test u[6] ≈ 1.0
        @test u[7] ≈ 1.0
    end

    let circ_obs = CircularObstacle([0.0, 2.0, 1.0], [π/2, π/2, 0], 1.0)
        u = [1.0, 0.0, 1.0, 1.0, 0.0, 1.0, 1.0]
        passed = pass!(u, circ_obs, p, out)
        @test passed === false
        @test u[1] ≈ 2.0
        @test u[2] ≈ 0.0
        @test u[3] ≈ 2.0
        @test u[4] ≈ 2.0
        @test u[5] ≈ 0.0
        @test u[6] ≈ 1.0
        @test u[7] ≈ 1.0
    end

    let circ_obs = CircularObstacle([0.0, 2.0, 1.0], [π/2, π/2, 0], 1.0-s)
        u = [1.0, 0.0, 1.0, 1.0, 0.0, 1.0, 1.0]
        passed = pass!(u, circ_obs, p, out)
        @test passed === true
        @test u[1] ≈ 2.0
        @test u[2] ≈ 0.0
        @test u[3] ≈ 2.0
        @test u[4] ≈ 2.0
        @test u[5] ≈ 0.0
        @test u[6] ≈ 1.0
        @test u[7] ≈ 1.0
    end
end


@testset "CircularSlit" begin
    let
        coordinates = SA[0.0, 0.0, 1.0]
        orientation = SA[0.0, 0.0, 0.0]
        radius = 2.0
        @test CircularSlit(coordinates, orientation, radius) isa CircularSlit
    end

    let
        coordinates = [0, 0, 1]
        orientation = [0, 0, 0]
        radius = 2
        @test CircularSlit(coordinates, orientation, radius) isa CircularSlit
    end

    let
        coordinates = [0, 0, 1]
        orientation = [0.0, 0.0, 0.0]
        radius = 2.0f0
        @test CircularSlit(coordinates, orientation, radius) isa CircularSlit
    end

    let
        coordinates = [0, 0, 1]
        radius = 2
        @test CircularSlit(coordinates, radius) isa CircularSlit
    end

    let circ_slit = CircularSlit([0.0, 0.0, 1.0], [0.0, 0.0, 0.0], 1.0)
        u = [0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0]
        passed = pass!(u, circ_slit, p, out)
        @test passed === true
        @test u[1] ≈ 1.0
        @test u[2] ≈ 0.0
        @test u[3] ≈ 1.0
        @test u[4] ≈ 1.0
        @test u[5] ≈ 0.0
        @test u[6] ≈ 1.0
        @test u[7] ≈ 1.0
    end

    let circ_slit = CircularSlit([0.0, 0.0, 1.0], [0.0, 0.0, 0.0], 1.0)
        u = [0.0, 0.0, -1.0, 0.0, 0.0, 1.0, 1.0]
        passed = pass!(u, circ_slit, p, out)
        @test passed === true
        @test u[1] ≈ 1.0
        @test u[2] ≈ 0.0
        @test u[3] ≈ 0.0
        @test u[4] ≈ 1.0
        @test u[5] ≈ 0.0
        @test u[6] ≈ 1.0
        @test u[7] ≈ 1.0
    end

    let circ_slit = CircularSlit([0.0, 0.0, 1.0], [0.0, 0.0, 0.0], 1.0)
        u = [0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 1.0]
        passed = pass!(u, circ_slit, p, out)
        @test passed === false
        @test u[1] ≈ 1.0
        @test u[2] ≈ 0.0
        @test u[3] ≈ 2.0
        @test u[4] ≈ 1.0
        @test u[5] ≈ 0.0
        @test u[6] ≈ 1.0
        @test u[7] ≈ 1.0
    end

    let circ_slit = CircularSlit([0.0, 2.0, 1.0], [π/2, π/2, 0], 1.0)
        u = [1.0, 0.0, 1.0, 1.0, 0.0, 1.0, 1.0]
        passed = pass!(u, circ_slit, p, out)
        @test passed === true
        @test u[1] ≈ 2.0
        @test u[2] ≈ 0.0
        @test u[3] ≈ 2.0
        @test u[4] ≈ 2.0
        @test u[5] ≈ 0.0
        @test u[6] ≈ 1.0
        @test u[7] ≈ 1.0
    end

    let circ_slit = CircularSlit([0.0, 2.0, 1.0], [π/2, π/2, 0], 1.0-s)
        u = [1.0, 0.0, 1.0, 1.0, 0.0, 1.0, 1.0]
        passed = pass!(u, circ_slit, p, out)
        @test passed === false
        @test u[1] ≈ 2.0
        @test u[2] ≈ 0.0
        @test u[3] ≈ 2.0
        @test u[4] ≈ 2.0
        @test u[5] ≈ 0.0
        @test u[6] ≈ 1.0
        @test u[7] ≈ 1.0
    end

    @test CircularSlit([0.0, 2.0, 1.0], 1.0) ==
            CircularSlit([0.0, 2.0, 1.0], (0, 0, 0), 1.0)
end


@testset "CircularDetectionPlane" begin
    let
        coordinates = SA[0.0, 0.0, 1.0]
        orientation = SA[0.0, 0.0, 0.0]
        radius = 2.0
        @test CircularDetectionPlane(coordinates, orientation, radius) isa CircularDetectionPlane
    end

    let
        coordinates = [0, 0, 1]
        orientation = [0, 0, 0]
        radius = 2
        @test CircularDetectionPlane(coordinates, orientation, radius) isa CircularDetectionPlane
    end

    let
        coordinates = [0, 0, 1]
        orientation = [0.0, 0.0, 0.0]
        radius = 2.0f0
        @test CircularDetectionPlane(coordinates, orientation, radius) isa CircularDetectionPlane
    end

    let
        coordinates = [0, 0, 1]
        radius = 2
        @test CircularDetectionPlane(coordinates, radius) isa CircularDetectionPlane
    end

    let circ_slit = CircularDetectionPlane([0.0, 0.0, 1.0], [0.0, 0.0, 0.0], 1.0)
        u = [0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0]
        passed = pass!(u, circ_slit, p, out)
        @test passed === true
        @test u[1] ≈ 1.0
        @test u[2] ≈ 0.0
        @test u[3] ≈ 1.0
        @test u[4] ≈ 1.0
        @test u[5] ≈ 0.0
        @test u[6] ≈ 1.0
        @test u[7] ≈ 1.0
    end

    let circ_slit = CircularDetectionPlane([0.0, 0.0, 1.0], [0.0, 0.0, 0.0], 1.0)
        u = [0.0, 0.0, -1.0, 0.0, 0.0, 1.0, 1.0]
        passed = pass!(u, circ_slit, p, out)
        @test passed === true
        @test u[1] ≈ 1.0
        @test u[2] ≈ 0.0
        @test u[3] ≈ 0.0
        @test u[4] ≈ 1.0
        @test u[5] ≈ 0.0
        @test u[6] ≈ 1.0
        @test u[7] ≈ 1.0
    end

    let circ_slit = CircularDetectionPlane([0.0, 0.0, 1.0], [0.0, 0.0, 0.0], 1.0)
        u = [0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 1.0]
        passed = pass!(u, circ_slit, p, out)
        @test passed === false
        @test u[1] ≈ 1.0
        @test u[2] ≈ 0.0
        @test u[3] ≈ 2.0
        @test u[4] ≈ 1.0
        @test u[5] ≈ 0.0
        @test u[6] ≈ 1.0
        @test u[7] ≈ 1.0
    end

    let circ_slit = CircularDetectionPlane([0.0, 2.0, 1.0], [π/2, π/2, 0], 1.0)
        u = [1.0, 0.0, 1.0, 1.0, 0.0, 1.0, 1.0]
        passed = pass!(u, circ_slit, p, out)
        @test passed === true
        @test u[1] ≈ 2.0
        @test u[2] ≈ 0.0
        @test u[3] ≈ 2.0
        @test u[4] ≈ 2.0
        @test u[5] ≈ 0.0
        @test u[6] ≈ 1.0
        @test u[7] ≈ 1.0
    end

    let circ_slit = CircularDetectionPlane([0.0, 2.0, 1.0], [π/2, π/2, 0], 1.0-s)
        u = [1.0, 0.0, 1.0, 1.0, 0.0, 1.0, 1.0]
        passed = pass!(u, circ_slit, p, out)
        @test passed === false
        @test u[1] ≈ 2.0
        @test u[2] ≈ 0.0
        @test u[3] ≈ 2.0
        @test u[4] ≈ 2.0
        @test u[5] ≈ 0.0
        @test u[6] ≈ 1.0
        @test u[7] ≈ 1.0
    end

    @test CircularDetectionPlane([0.0, 2.0, 1.0], 1.0) ==
            CircularDetectionPlane([0.0, 2.0, 1.0], (0, 0, 0), 1.0)
end


@testset "RectangularSlit" begin
    let
        coordinates = SA[0.0, 0.0, 0.0]
        orientation = SA[0.0, 0.0, 0.0]
        width = 1.0
        height = 1.0
        @test RectangularSlit(coordinates, orientation, width, height) isa RectangularSlit
    end

    let
        coordinates = [0.0, 0.0, 0.0]
        orientation = [0.0, 0.0, 0.0]
        width = 1.0
        height = 1.0
        @test RectangularSlit(coordinates, orientation, width, height) isa RectangularSlit
    end

    let
        coordinates = [0.0, 0, 0]
        orientation = [0.0, 0.0, 0]
        width = 1.0f0
        height = 1.0
        @test RectangularSlit(coordinates, orientation, width, height) isa RectangularSlit
    end

    let
        coordinates = [0.0, 0.0, 0.0]
        width = 1.0
        height = 1.0
        @test RectangularSlit(coordinates, width, height) isa RectangularSlit
    end

    let rec_slit = RectangularSlit([0.0, 0.0, 1.0], [0.0, 0.0, 0.0], 2.0, 1.0)
        u = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0]
        passed = pass!(u, rec_slit, p, out)
        @test passed === true
        @test u[1] ≈ 1.0
        @test u[2] ≈ 0.0
        @test u[3] ≈ 0.0
        @test u[4] ≈ 1.0
        @test u[5] ≈ 0.0
        @test u[6] ≈ 0.0
        @test u[7] ≈ 1.0
    end

    let rec_slit = RectangularSlit([0.0, 0.0, 1.0], [0.0, 0.0, 0.0], 2.0, 1.0)
        u = [0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 1.0]
        passed = pass!(u, rec_slit, p, out)
        @test passed === true
        @test u[1] ≈ 1.0
        @test u[2] ≈ 1.0
        @test u[3] ≈ 0.0
        @test u[4] ≈ 1.0
        @test u[5] ≈ 1.0
        @test u[6] ≈ 0.0
        @test u[7] ≈ 1.0
    end

    let rec_slit = RectangularSlit([0.0, 0.0, 1.0], [0.0, 0.0, 0.0], 2.0, 1.0)
        u = [0.0, 0.0, 0.0, 0.0, 0.0, 0.5, 1.0]
        passed = pass!(u, rec_slit, p, out)
        @test passed === true
        @test u[1] ≈ 1.0
        @test u[2] ≈ 0.0
        @test u[3] ≈ 0.5
        @test u[4] ≈ 1.0
        @test u[5] ≈ 0.0
        @test u[6] ≈ 0.5
        @test u[7] ≈ 1.0
    end

    let rec_slit = RectangularSlit([0.0, 0.0, 1.0], [0.0, 0.0, 0.0], 2.0, 1.0)
        u = [0.0, 0.0, 0.0, 0.0, 1.0, 0.5, 1.0]
        passed = pass!(u, rec_slit, p, out)
        @test passed === true
        @test u[1] ≈ 1.0
        @test u[2] ≈ 1.0
        @test u[3] ≈ 0.5
        @test u[4] ≈ 1.0
        @test u[5] ≈ 1.0
        @test u[6] ≈ 0.5
        @test u[7] ≈ 1.0
    end

    let rec_slit = RectangularSlit([0.0, 0.0, 1.0], [0.0, 0.0, 0.0], 2.0, 1.0)
        u = [0.0, 0.0, 0.0, 0.0, 1.0+s, 0.0, 1.0]
        passed = pass!(u, rec_slit, p, out)
        @test passed === false
        @test u[1] ≈ 1.0
        @test u[2] ≈ 1.0+s
        @test u[3] ≈ 0.0
        @test u[4] ≈ 1.0
        @test u[5] ≈ 1.0+s
        @test u[6] ≈ 0.0
        @test u[7] ≈ 1.0
    end

    let rec_slit = RectangularSlit([0.0, 0.0, 1.0], [0.0, 0.0, 0.0], 2.0, 1.0)
        u = [0.0, 0.0, 0.0, 0.0, 0.0, 0.5+s, 1.0]
        passed = pass!(u, rec_slit, p, out)
        @test passed === false
        @test u[1] ≈ 1.0
        @test u[2] ≈ 0.0
        @test u[3] ≈ 0.5+s
        @test u[4] ≈ 1.0
        @test u[5] ≈ 0.0
        @test u[6] ≈ 0.5+s
        @test u[7] ≈ 1.0
    end

    let rec_slit = RectangularSlit([0.0, 0.0, 1.0], [0.0, 0.0, 0.0], 2.0, 1.0)
        u = [0.0, 0.0, 0.0, 0.0, 1.0+s, 0.5+s, 1.0]
        passed = pass!(u, rec_slit, p, out)
        @test passed === false
        @test u[1] ≈ 1.0
        @test u[2] ≈ 1.0+s
        @test u[3] ≈ 0.5+s
        @test u[4] ≈ 1.0
        @test u[5] ≈ 1.0+s
        @test u[6] ≈ 0.5+s
        @test u[7] ≈ 1.0
    end

    let rec_slit = RectangularSlit([1.0, 2.0, 1.0], [π/2, π/2, -π/2], 2.0, 1.0+s)
        u = [1.0, 0.0, 1.0, 0.5, 0.0, 1.0, 0.0]
        passed = pass!(u, rec_slit, p, out)
        @test passed === true
        @test u[1] ≈ 2.0
        @test u[2] ≈ 0.0
        @test u[3] ≈ 2.0
        @test u[4] ≈ 0.5
        @test u[5] ≈ 0.0
        @test u[6] ≈ 1.0
        @test u[7] ≈ 0.0
    end

    let rec_slit = RectangularSlit([1.0, 2.0, 1.0], [π/2, π/2, 0], 2.0, 1.0)
        u = [1.0, 0.0-s, 1.0, 0.5, 0.0, 1.0, 0.0]
        passed = pass!(u, rec_slit, p, out)
        @test passed === false
        @test u[1] ≈ 2.0
        @test u[2] ≈ 0.0-s
        @test u[3] ≈ 2.0
        @test u[4] ≈ 0.5
        @test u[5] ≈ 0.0
        @test u[6] ≈ 1.0
        @test u[7] ≈ 0.0
    end

    @test RectangularSlit([1.0, 2.0, 1.0], 2.0, 1.0) ==
            RectangularSlit([1.0, 2.0, 1.0], (0, 0, 0), 2.0, 1.0)
end


@testset "RectangularDetectionPlane" begin
    let
        coordinates = SA[0.0, 0.0, 0.0]
        orientation = SA[0.0, 0.0, 0.0]
        width = 1.0
        height = 1.0
        @test RectangularDetectionPlane(coordinates, orientation, width, height) isa RectangularDetectionPlane
    end

    let
        coordinates = [0.0, 0.0, 0.0]
        orientation = [0.0, 0.0, 0.0]
        width = 1.0
        height = 1.0
        @test RectangularDetectionPlane(coordinates, orientation, width, height) isa RectangularDetectionPlane
    end

    let
        coordinates = [0.0, 0, 0]
        orientation = [0.0, 0.0, 0]
        width = 1.0f0
        height = 1.0
        @test RectangularDetectionPlane(coordinates, orientation, width, height) isa RectangularDetectionPlane
    end

    let
        coordinates = [0.0, 0.0, 0.0]
        width = 1.0
        height = 1.0
        @test RectangularDetectionPlane(coordinates, width, height) isa RectangularDetectionPlane
    end

    let rec_det = RectangularDetectionPlane([0.0, 0.0, 1.0], [0.0, 0.0, 0.0], 2.0, 1.0)
        u = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0]
        passed = pass!(u, rec_det, p, out)
        @test passed === true
        @test u[1] ≈ 1.0
        @test u[2] ≈ 0.0
        @test u[3] ≈ 0.0
        @test u[4] ≈ 1.0
        @test u[5] ≈ 0.0
        @test u[6] ≈ 0.0
        @test u[7] ≈ 1.0
    end

    let rec_det = RectangularDetectionPlane([0.0, 0.0, 1.0], [0.0, 0.0, 0.0], 2.0, 1.0)
        u = [0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 1.0]
        passed = pass!(u, rec_det, p, out)
        @test passed === true
        @test u[1] ≈ 1.0
        @test u[2] ≈ 1.0
        @test u[3] ≈ 0.0
        @test u[4] ≈ 1.0
        @test u[5] ≈ 1.0
        @test u[6] ≈ 0.0
        @test u[7] ≈ 1.0
    end

    let rec_det = RectangularDetectionPlane([0.0, 0.0, 1.0], [0.0, 0.0, 0.0], 2.0, 1.0)
        u = [0.0, 0.0, 0.0, 0.0, 0.0, 0.5, 1.0]
        passed = pass!(u, rec_det, p, out)
        @test passed === true
        @test u[1] ≈ 1.0
        @test u[2] ≈ 0.0
        @test u[3] ≈ 0.5
        @test u[4] ≈ 1.0
        @test u[5] ≈ 0.0
        @test u[6] ≈ 0.5
        @test u[7] ≈ 1.0
    end

    let rec_det = RectangularDetectionPlane([0.0, 0.0, 1.0], [0.0, 0.0, 0.0], 2.0, 1.0)
        u = [0.0, 0.0, 0.0, 0.0, 1.0, 0.5, 1.0]
        passed = pass!(u, rec_det, p, out)
        @test passed === true
        @test u[1] ≈ 1.0
        @test u[2] ≈ 1.0
        @test u[3] ≈ 0.5
        @test u[4] ≈ 1.0
        @test u[5] ≈ 1.0
        @test u[6] ≈ 0.5
        @test u[7] ≈ 1.0
    end

    let rec_det = RectangularDetectionPlane([0.0, 0.0, 1.0], [0.0, 0.0, 0.0], 2.0, 1.0)
        u = [0.0, 0.0, 0.0, 0.0, 1.0+s, 0.0, 1.0]
        passed = pass!(u, rec_det, p, out)
        @test passed === false
        @test u[1] ≈ 1.0
        @test u[2] ≈ 1.0+s
        @test u[3] ≈ 0.0
        @test u[4] ≈ 1.0
        @test u[5] ≈ 1.0+s
        @test u[6] ≈ 0.0
        @test u[7] ≈ 1.0
    end

    let rec_det = RectangularDetectionPlane([0.0, 0.0, 1.0], [0.0, 0.0, 0.0], 2.0, 1.0)
        u = [0.0, 0.0, 0.0, 0.0, 0.0, 0.5+s, 1.0]
        passed = pass!(u, rec_det, p, out)
        @test passed === false
        @test u[1] ≈ 1.0
        @test u[2] ≈ 0.0
        @test u[3] ≈ 0.5+s
        @test u[4] ≈ 1.0
        @test u[5] ≈ 0.0
        @test u[6] ≈ 0.5+s
        @test u[7] ≈ 1.0
    end

    let rec_det = RectangularDetectionPlane([0.0, 0.0, 1.0], [0.0, 0.0, 0.0], 2.0, 1.0)
        u = [0.0, 0.0, 0.0, 0.0, 1.0+s, 0.5+s, 1.0]
        passed = pass!(u, rec_det, p, out)
        @test passed === false
        @test u[1] ≈ 1.0
        @test u[2] ≈ 1.0+s
        @test u[3] ≈ 0.5+s
        @test u[4] ≈ 1.0
        @test u[5] ≈ 1.0+s
        @test u[6] ≈ 0.5+s
        @test u[7] ≈ 1.0
    end

    let rec_det = RectangularDetectionPlane([1.0, 2.0, 1.0], [π/2, π/2, -π/2], 2.0, 1.0+s)
        u = [1.0, 0.0, 1.0, 0.5, 0.0, 1.0, 0.0]
        passed = pass!(u, rec_det, p, out)
        @test passed === true
        @test u[1] ≈ 2.0
        @test u[2] ≈ 0.0
        @test u[3] ≈ 2.0
        @test u[4] ≈ 0.5
        @test u[5] ≈ 0.0
        @test u[6] ≈ 1.0
        @test u[7] ≈ 0.0
    end

    let rec_det = RectangularDetectionPlane([1.0, 2.0, 1.0], [π/2, π/2, 0], 2.0, 1.0)
        u = [2.0, 0.0-s, 1.0, 0.5, 0.0, 1.0, 0.0]
        passed = pass!(u, rec_det, p, out)
        @test passed === false
        @test u[1] ≈ 3.0
        @test u[2] ≈ 0.0-s
        @test u[3] ≈ 2.0
        @test u[4] ≈ 0.5
        @test u[5] ≈ 0.0
        @test u[6] ≈ 1.0
        @test u[7] ≈ 0.0
    end

    @test RectangularDetectionPlane([1.0, 2.0, 1.0], 2.0, 1.0) ==
            RectangularDetectionPlane([1.0, 2.0, 1.0], (0, 0, 0), 2.0, 1.0)
end


@testset "FunctionNozzle" begin
    let
        function nozzlefunction!(u, i, particle)
            n = 3
            i > n && return false

            t0 = 0.0
            x0, y0, z0 = 0.0, 0.0, 0.0
            v0 = 500.0
            θi = -1
            θf = 1
            θ = θi + (i - 1)*(θf - θi)/(n - 1)
            u[1] = t0
            u[2] = x0
            u[3] = y0
            u[4] = z0
            u[5] = 0.0
            u[6] = v0*sind(θ)
            u[7] = v0*cosd(θ)
            return true
        end
        nozzle = FunctionNozzle(nozzlefunction!)
        u = zeros(Float64, (7,))
        @test emit!(u, 1, nozzle, p) == true
        @test u[6] ≈ 500.0*sind(-1.0)
        @test u[7] ≈ 500.0*cosd(-1.0)
        @test emit!(u, 2, nozzle, p) == true
        @test u[6] ≈ 500.0*sind(0.0)
        @test u[7] ≈ 500.0*cosd(0.0)
        @test emit!(u, 3, nozzle, p) == true
        @test u[6] ≈ 500.0*sind(1.0)
        @test u[7] ≈ 500.0*cosd(1.0)
        @test emit!(u, 4, nozzle, p) == false

        @test isnothing(length(nozzle))
    end
end


@testset "IteratorNozzle" begin
    let
        t0 = 0.0
        x0, y0, z0 = 0.0, 0.0, 0.0
        v0 = 500.0
        iter = [[t0, x0, y0, z0, 0.0, v0*sind(θ), v0*cosd(θ)] for θ in -1.0:1:1.0]
        nozzle = IteratorNozzle(iter)
        u = zeros(Float64, (7,))
        @test emit!(u, 1, nozzle, p) == true
        @test u[6] ≈ 500.0*sind(-1.0)
        @test u[7] ≈ 500.0*cosd(-1.0)
        @test emit!(u, 2, nozzle, p) == true
        @test u[6] ≈ 500.0*sind(0.0)
        @test u[7] ≈ 500.0*cosd(0.0)
        @test emit!(u, 3, nozzle, p) == true
        @test u[6] ≈ 500.0*sind(1.0)
        @test u[7] ≈ 500.0*cosd(1.0)
        @test emit!(u, 4, nozzle, p) == false

        @test length(nozzle) == length(iter)
    end
end


@testset "RandomPointNozzle" begin
    let
        t0 = 0.0
        coord = (0.0, 0.0, 0.0)
        orien = (0.0, 0.0, 0.0)
        n = 3
        vmin = 480.0
        vmax = 520.0
        nozzle = RandomPointNozzle(coord, orien, n, vmin, vmax, 1.0)
        u = zeros(Float64, (7,))
        @test emit!(u, 1, nozzle, p) == true
        @test vmin ≤ norm(u[5:7]) ≤ vmax
        @test acosd(u[7]/norm(u[5:7])) ≤ 1.0

        @test emit!(u, 2, nozzle, p) == true
        @test vmin ≤ norm(u[5:7]) ≤ vmax
        @test acosd(u[7]/norm(u[5:7])) ≤ 1.0

        @test emit!(u, 3, nozzle, p) == true
        @test vmin ≤ norm(u[5:7]) ≤ vmax
        @test acosd(u[7]/norm(u[5:7])) ≤ 1.0

        @test emit!(u, 4, nozzle, p) == false

        vdist = Uniform(vmin, vmax)
        @test RandomPointNozzle(coord, orien, n, vdist, 1.0) isa RandomPointNozzle

        @test RandomPointNozzle(coord, n, vmin, vmax, 1.0) ==
                RandomPointNozzle(coord, (0, 0, 0), n, vmin, vmax, 1.0)

        @test length(nozzle) == n
    end
end
