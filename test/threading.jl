using Random: default_rng
using Test
using ParticleTrajectorySimulationsCore


@testset "threading" begin
    SINGLETHREAD = tempname()
    MULTITHREADS = tempname()
    function tempfiles(f)
        try
            f(SINGLETHREAD, MULTITHREADS)
        finally
            rm(SINGLETHREAD)
            rm(MULTITHREADS)
        end
    end

    n = 100
    v0 = 500
    θmax = 0.3
    itr = [[0.0, 0.0, 0.0, 0.0, 0.0, v0*sind(θ), v0*cosd(θ)] for θ in range(0.0, θmax, n)]
    nozzle = IteratorNozzle(itr)
    hexapole = Hexapole((0.0, 0.0, 1.0), (0, 0, 0), 0.5, 0.005, 6000.0)
    apprts = SimpleApparatus(nozzle, [hexapole])
    p = BasicSymmetricTop1stOrder("methyl iodide", 141.94, 0.25, 1.6406,  1, 1, -1)

    tempfiles() do single, multi
        # Almost trajectories will success but a few trajectories will fail.
        apprts = SimpleApparatus(IteratorNozzle(copy(itr)), [hexapole])
        ParticleTrajectorySimulationsCore.run(apprts, p, log=single, num_threads=1)

        apprts = SimpleApparatus(IteratorNozzle(copy(itr)), [hexapole])
        ParticleTrajectorySimulationsCore.run(apprts, p, log=multi, num_threads=Threads.nthreads())

        for (t_single, t_multi) in zip(collect_trajectories(single), collect_trajectories(multi))
            @test all(t_single .≈ t_multi)
        end
    end
end
